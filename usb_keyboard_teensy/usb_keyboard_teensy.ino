// Normal keyboard keys
char en_number_array[10] = {'KEYPAD_1','KEYPAD_2','KEYPAD_3','KEYPAD_4','KEYPAD_5','KEYPAD_6','KEYPAD_7','KEYPAD_8','KEYPAD_9','KEYPAD_0'};
// Normal keyboard keys
char en_letter_array[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
// Use ascii characters for special characters: - = [ ] \ ; ' ` , . /
char en_special_array[11] = {0x2D,0x3D,0x5B,0x5D,0x5C,0x3B,0x27,0x60,0x2C,0x2E,0x2F};

/*
Run everything through setup so that it is only run once per bootup.
*/
void setup() {
  // Enable the keyboard and mouse for use later.
  Keyboard.begin();
  Mouse.begin();
  // Set the pins needed for the display to be set for output  
  pinMode(PIN_E7, OUTPUT); 
  pinMode(PIN_E6, OUTPUT);
  pinMode(PIN_D7, OUTPUT);
  pinMode(PIN_D5, OUTPUT);
  pinMode(PIN_D4, OUTPUT);
  pinMode(PIN_D4, OUTPUT);
  pinMode(PIN_B0, OUTPUT);
  pinMode(PIN_B1, OUTPUT);
  // Input a number for how many seconds you want the loop to go
  // This will allow the driver to fully load.
  display_idle(10);
  // Countdown from 9 to 0 and begin doing work.
  display_countdown();
  // Clear the display
  char_null();

  
  // Move the mouse to the upper left corner of the screen
  mouse_reset();
  // Move the mouse to a specific coordinates
  // This is for demo purposes so far, no praticle use YET.
  mouse_move(400,200);
  
  // Que up the individual tests you want to accomplish.
  /* 
  Test 1 involves the following tests:
     start 2 instances of notepad to alt+tab between them later.
     1-0 from number pad with numlock on (done twice)
     a-z, 1-0, and special characters
     a-z, 1-0, and special characters with caps lock on
     a-z, 1-0, and special characters with shift on
     ctrl+a, ctrl+c to select all text and copy to clipboard
  Options:
     first option is for how many words per minute you want it to type at.
       NOTE: Guiness book of world records states the fasts was around 180 words per minute, this is our baseline
     second option is how many times the character "1" will be typed onto the two notepad docs before we are done.
  */    
  test_1(180,130000);
  
  /* 
  Test 2 involves the following tests:
     print out large amounts of text
     part way through, you can mess with backspace and re-typing a word(s)
  */    
  test_2();
}
void loop() {
  // This will continuely loop through since its in the main system loop. It will never stop.
  display_idle(1);
}

/* 
####################################################################
Start Defining what tests you want to perform
####################################################################
*/

void test_1(int type_speed, long end_number){
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad twice so we can alt+tab between them.
  start_process("notepad");
  delay(1000);
  start_process("notepad");
  delay(1000);

  // Test out numbpad keys first.
  // We will run twice to ensure numlock is turned on at least once.
  keyboard_press(KEY_NUM_LOCK,0);
  delay(delay_time);
  keyboard_press(KEYPAD_1,0);
  delay(delay_time);
  keyboard_press(KEYPAD_2,0);
  delay(delay_time);
  keyboard_press(KEYPAD_3,0);
  delay(delay_time);
  keyboard_press(KEYPAD_4,0);
  delay(delay_time);
  keyboard_press(KEYPAD_5,0);
  delay(delay_time);
  keyboard_press(KEYPAD_6,0);
  delay(delay_time);
  keyboard_press(KEYPAD_7,0);
  delay(delay_time);
  keyboard_press(KEYPAD_8,0);
  delay(delay_time);
  keyboard_press(KEYPAD_9,0);
  delay(delay_time);
  keyboard_press(KEYPAD_0,0);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);
  keyboard_press(KEY_NUM_LOCK,0);
  delay(delay_time);
  keyboard_press(KEYPAD_1,0);
  delay(delay_time);
  keyboard_press(KEYPAD_2,0);
  delay(delay_time);
  keyboard_press(KEYPAD_3,0);
  delay(delay_time);
  keyboard_press(KEYPAD_4,0);
  delay(delay_time);
  keyboard_press(KEYPAD_5,0);
  delay(delay_time);
  keyboard_press(KEYPAD_6,0);
  delay(delay_time);
  keyboard_press(KEYPAD_7,0);
  delay(delay_time);
  keyboard_press(KEYPAD_8,0);
  delay(delay_time);
  keyboard_press(KEYPAD_9,0);
  delay(delay_time);
  keyboard_press(KEYPAD_0,0);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);

  // Start typing the keys on the keyboard
  keyboard_press(KEY_A,0);
  delay(delay_time);
  keyboard_press(KEY_B,0);
  delay(delay_time);
  keyboard_press(KEY_C,0);
  delay(delay_time);
  keyboard_press(KEY_D,0);
  delay(delay_time);
  keyboard_press(KEY_E,0);
  delay(delay_time);
  keyboard_press(KEY_F,0);
  delay(delay_time);
  keyboard_press(KEY_G,0);
  delay(delay_time);
  keyboard_press(KEY_H,0);
  delay(delay_time);
  keyboard_press(KEY_I,0);
  delay(delay_time);
  keyboard_press(KEY_J,0);
  delay(delay_time);
  keyboard_press(KEY_K,0);
  delay(delay_time);
  keyboard_press(KEY_L,0);
  delay(delay_time);
  keyboard_press(KEY_M,0);
  delay(delay_time);
  keyboard_press(KEY_N,0);
  delay(delay_time);
  keyboard_press(KEY_O,0);
  delay(delay_time);
  keyboard_press(KEY_P,0);
  delay(delay_time);
  keyboard_press(KEY_Q,0);
  delay(delay_time);
  keyboard_press(KEY_R,0);
  delay(delay_time);
  keyboard_press(KEY_S,0);
  delay(delay_time);
  keyboard_press(KEY_T,0);
  delay(delay_time);
  keyboard_press(KEY_U,0);
  delay(delay_time);
  keyboard_press(KEY_V,0);
  delay(delay_time);
  keyboard_press(KEY_W,0);
  delay(delay_time);
  keyboard_press(KEY_X,0);
  delay(delay_time);
  keyboard_press(KEY_Y,0);
  delay(delay_time);
  keyboard_press(KEY_Z,0);
  delay(delay_time);
  keyboard_press(KEY_1,0);
  delay(delay_time);
  keyboard_press(KEY_2,0);
  delay(delay_time);
  keyboard_press(KEY_3,0);
  delay(delay_time);
  keyboard_press(KEY_4,0);
  delay(delay_time);
  keyboard_press(KEY_5,0);
  delay(delay_time);
  keyboard_press(KEY_6,0);
  delay(delay_time);
  keyboard_press(KEY_7,0);
  delay(delay_time);
  keyboard_press(KEY_8,0);
  delay(delay_time);
  keyboard_press(KEY_9,0);
  delay(delay_time);
  keyboard_press(KEY_0,0);
  delay(delay_time);
  keyboard_press(KEY_MINUS,0);
  delay(delay_time);
  keyboard_press(KEY_EQUAL,0);
  delay(delay_time);
  keyboard_press(KEY_LEFT_BRACE,0);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_BRACE,0);
  delay(delay_time);
  keyboard_press(KEY_BACKSLASH,0);
  delay(delay_time);
  keyboard_press(KEY_SEMICOLON,0);
  delay(delay_time);
  keyboard_press(KEY_QUOTE,0);
  delay(delay_time);
  keyboard_press(KEY_TILDE,0);
  delay(delay_time);
  keyboard_press(KEY_COMMA,0);
  delay(delay_time);
  keyboard_press(KEY_PERIOD,0);
  delay(delay_time);
  keyboard_press(KEY_SLASH,0);
  delay(delay_time);
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);
  
  
  // Same thing as above, but with caps lock on
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press(KEY_A,0);
  delay(delay_time);
  keyboard_press(KEY_B,0);
  delay(delay_time);
  keyboard_press(KEY_C,0);
  delay(delay_time);
  keyboard_press(KEY_D,0);
  delay(delay_time);
  keyboard_press(KEY_E,0);
  delay(delay_time);
  keyboard_press(KEY_F,0);
  delay(delay_time);
  keyboard_press(KEY_G,0);
  delay(delay_time);
  keyboard_press(KEY_H,0);
  delay(delay_time);
  keyboard_press(KEY_I,0);
  delay(delay_time);
  keyboard_press(KEY_J,0);
  delay(delay_time);
  keyboard_press(KEY_K,0);
  delay(delay_time);
  keyboard_press(KEY_L,0);
  delay(delay_time);
  keyboard_press(KEY_M,0);
  delay(delay_time);
  keyboard_press(KEY_N,0);
  delay(delay_time);
  keyboard_press(KEY_O,0);
  delay(delay_time);
  keyboard_press(KEY_P,0);
  delay(delay_time);
  keyboard_press(KEY_Q,0);
  delay(delay_time);
  keyboard_press(KEY_R,0);
  delay(delay_time);
  keyboard_press(KEY_S,0);
  delay(delay_time);
  keyboard_press(KEY_T,0);
  delay(delay_time);
  keyboard_press(KEY_U,0);
  delay(delay_time);
  keyboard_press(KEY_V,0);
  delay(delay_time);
  keyboard_press(KEY_W,0);
  delay(delay_time);
  keyboard_press(KEY_X,0);
  delay(delay_time);
  keyboard_press(KEY_Y,0);
  delay(delay_time);
  keyboard_press(KEY_Z,0);
  delay(delay_time);
  keyboard_press(KEY_1,0);
  delay(delay_time);
  keyboard_press(KEY_2,0);
  delay(delay_time);
  keyboard_press(KEY_3,0);
  delay(delay_time);
  keyboard_press(KEY_4,0);
  delay(delay_time);
  keyboard_press(KEY_5,0);
  delay(delay_time);
  keyboard_press(KEY_6,0);
  delay(delay_time);
  keyboard_press(KEY_7,0);
  delay(delay_time);
  keyboard_press(KEY_8,0);
  delay(delay_time);
  keyboard_press(KEY_9,0);
  delay(delay_time);
  keyboard_press(KEY_0,0);
  delay(delay_time);
  keyboard_press(KEY_MINUS,0);
  delay(delay_time);
  keyboard_press(KEY_EQUAL,0);
  delay(delay_time);
  keyboard_press(KEY_LEFT_BRACE,0);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_BRACE,0);
  delay(delay_time);
  keyboard_press(KEY_BACKSLASH,0);
  delay(delay_time);
  keyboard_press(KEY_SEMICOLON,0);
  delay(delay_time);
  keyboard_press(KEY_QUOTE,0);
  delay(delay_time);
  keyboard_press(KEY_TILDE,0);
  delay(delay_time);
  keyboard_press(KEY_COMMA,0);
  delay(delay_time);
  keyboard_press(KEY_PERIOD,0);
  delay(delay_time);
  keyboard_press(KEY_SLASH,0);
  delay(delay_time);
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);
  
  // Run through the keyboard keys with the shift key on.
  keyboard_press(KEY_A,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_B,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_C,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_D,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_E,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_F,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_G,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_H,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_I,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_J,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_K,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_L,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_M,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_N,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_O,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_P,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_Q,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_R,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_S,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_T,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_U,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_V,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_W,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_X,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_Y,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_Z,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_1,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_2,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_3,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_4,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_5,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_6,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_7,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_8,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_9,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_0,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_MINUS,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_EQUAL,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_LEFT_BRACE,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_BRACE,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_BACKSLASH,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_SEMICOLON,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_QUOTE,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_TILDE,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_COMMA,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_PERIOD,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_SLASH,MODIFIERKEY_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);
  
  // Copy everything that has been written to the clipboard
  keyboard_press(KEY_A,MODIFIERKEY_CTRL);
  delay(delay_time);
  keyboard_press(KEY_C,MODIFIERKEY_CTRL);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  
  // Start repeating the character "1" over and over again for the specified number of times.
  // After every 100, alt-tab to the other notepad window and hit enter and continue.
  long x=0;
  int z=0;
  do {
    keyboard_press(KEY_1,0);
    delay(delay_time);
    x=x+1;
    if (z < 100) {
      z=z+1;
    }
    else {
      keyboard_press(KEY_TAB,MODIFIERKEY_ALT);
      delay(500);
      z=0;
      keyboard_press(KEY_ENTER,0);
      delay(delay_time);
    }
  } while ( x < end_number );
}

void test_english(int type_speed){
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad so we can type in it.
  start_process("notepad");
  delay(1000);
  
  // Test out number keys first using the number array from above.
  for (byte i=0; i <= 9; i++){
    keyboard_press(en_number_array[i],0);
    delay(delay_time);
  }
  keyboard_press(KEY_ENTER,0);
  delay(delay_time);
  
  // Now number keys with shift on
  for (byte i=0; i <= 9; i++){
    keyboard_press(en_number_array[i],KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);

  // Test out arrow keys just to verify
  keyboard_press(KEY_UP_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_LEFT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_DOWN_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Start typing the keys on the keyboard
  for (byte i=0; i <= 25; i++){
    keyboard_press(en_letter_array[i],0);
    delay(delay_time);
  }
  //Start typing special keys
  for (byte i=0; i <= 10; i++){
    keyboard_press(&en_special_array[i],0);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Same thing as above, but with caps lock on
  keyboard_press(KEY_CAPS_LOCK,0);
  for (byte i=0; i <= 25; i++){
    keyboard_press(en_letter_array[i],0);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    keyboard_press(en_special_array[i],0);
    delay(delay_time);
  }
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // One last time, this time with the left shift key pressed for each key.
  for (byte i=0; i <= 25; i++){
    keyboard_press(en_letter_array[i],KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    keyboard_press(en_special_array[i],KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Copy everything that has been written to the clipboard
  keyboard_press('a',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press('c',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  
  close_current_process();
}





// owl is a good keyword to search for in this text
void test_2(){
  // Start notepad so we can type into it.
  start_process("notepad");
  delay(1000);
  
  Keyboard.print("You don't know about me without you have read a book by the name of The Adventures of Tom Sawyer; but that ain't no matter. That book was made by Mr. Mark Twain, and he told the truth, mainly. There was things which he stretched, but mainly he told the truth. ");
  Keyboard.print("That is nothing. I never seen anybody but lied one time or another, without it was Aunt Polly, or the widow, or maybe Mary. Aunt Polly -- Tom's Aunt Polly, she is -- and Mary, and the Widow Douglas is all told about in that book, which is mostly a true book, ");
  Keyboard.print("with some stretchers, as I said before. Now the way that the book winds up is this: Tom and me found the money that the robbers hid in the cave, and it made us rich. We got six thousand dollars apiece -- all gold. It was an awful sight of money when ");
  Keyboard.print("it was piled up. Well, Judge Thatcher he took it and put it out at interest, and it fetched us a dollar a day apiece all the year round -- more than a body could tell what to do with. The Widow Douglas she took me for her son, and allowed she would sivilize me; ");
  Keyboard.print("but it was rough living in the house all the time, considering how dismal regular and decent the widow was in all her ways; and so when I couldn't stand it no longer I lit out. I got into my old rags and my sugar-hogshead again, and was free and satisfied. But ");
  Keyboard.print("Tom Sawyer he hunted me up and said he was going to start a band of robbers, and I might join if I would go back to the widow and be respectable. So I went back. The widow she cried over me, and called me a poor lost lamb, and she called me a lot of other ");
  Keyboard.print("names, too, but she never meant no harm by it. She put me in them new clothes again, and I couldn't do nothing but sweat and sweat, and feel all cramped up. Well, then, the old thing commenced again. The widow rung a bell for supper, and you had to come to ");
  Keyboard.print("time. When you got to the table you couldn't go right to eating, but you had to wait for the widow to tuck down her head and grumble a little over the victuals, though there warn't really anything the matter with them, -- that is, nothing only everything was ");
  Keyboard.print("cooked by itself. In a barrel of odds and ends it is different; things get mixed up, and the juice kind of swaps around, and the things go better. After supper she got out her book and learned me about Moses and the Bulrushers, and I was in a sweat to ");
  Keyboard.print("find out all about him; but by and by she let it out that Moses had been dead a considerable long time; so then I didn't care no more about him, because I don't take no stock in dead people. Pretty soon I wanted to smoke, and asked the widow to let me. But ");
  Keyboard.print("she wouldn't. She said it was a mean practice and wasn't clean, and I must try to not do it any more. That is just the way with some people. They get down on a thing when they don't know nothing about it. Here she was a-bothering about Moses, which was no kin to ");
  Keyboard.print("her, and no use to any- body, being gone, you see, yet finding a power of fault with me for doing a thing that had some good in it. And she took snuff, too; of course that was all right, because she done it herself. Her sister, Miss Watson, a tolerable ");
  Keyboard.print("slim old maid, with goggles on, had just come to live with her, and took a set at me now with a spelling-book. She worked me middling hard for about an hour, and then the widow made her ease up. I couldn't stood it much longer. Then for an hour it was deadly ");
  Keyboard.print("dull, and I was fidgety. Miss Watson would say, 'Don't put your feet up there, Huckleberry;' and 'Don't scrunch up like that, Huckleberry -- set up straight;' and pretty soon she would say, 'Don't gap and stretch like that, Huckleberry -- why don't you try to ");
  Keyboard.print("be-have?' Then she told me all about the bad place, and I said I wished I was there. She got mad then, but I didn't mean no harm. All I wanted was to go somewheres; all I wanted was a change, I warn't particular. She said it was wicked to say what I said; said ");
  Keyboard.print("she wouldn't say it for the whole world; she was going to live so as to go to the good place. Well, I couldn't see no advantage in going where she was going, so I made up my mind I wouldn't try for it. But I never said so, because it would only make trouble, and ");
  Keyboard.print("wouldn't do no good. Now she had got a start, and she went on and told me all about the good place. She said all a body would have to do there was to go around all day long with a harp and sing, forever and ever. So I didn't think much of it. But I never ");
  Keyboard.print("said so. I asked her if she reckoned Tom Sawyer would go there, and she said not by a considerable sight. I was glad about that, because I wanted him and me to be together. Miss Watson she kept pecking at me, and it got tiresome and lonesome. By and by ");
  Keyboard.print("they fetched the niggers in and had prayers, and then everybody was off to bed. I went up to my room with a piece of candle, and put it on the table. Then I set down in a chair by the window and tried to think of something cheerful, but it warn't no use. I felt ");
  Keyboard.print("so lonesome I most wished I was dead. The stars were shining, and the leaves rustled in the woods ever so mournful; and I heard an ");
  //here is your keyword, you can print it as is, or use the backspace keys to modify it in real time.
  Keyboard.print("owl");
  Keyboard.print(", away off, who-whooing about some- body that was dead, and a whippowill and a dog cry- ing about somebody that was going to ");
  Keyboard.print("die; and the wind was trying to whisper something to me, and I couldn't make out what it was, and so it made the cold shivers run over me. Then away out in the woods I heard that kind of a sound that a ghost makes when it wants to tell about something that's on ");
  Keyboard.print("its mind and can't make itself understood, and so can't rest easy in its grave, and has to go about that way every night grieving. I got so down-hearted and scared I did wish I had some company. Pretty soon a spider went crawling up my shoulder, and I flipped it ");
  Keyboard.print("off and it lit in the candle; and before I could budge it was all shriveled up. I didn't need anybody to tell me that that was an awful bad sign and would fetch me some bad luck, so I was scared and most shook the clothes off of me. I got up and turned around in ");
  Keyboard.print("my tracks three times and crossed my breast every time; and then I tied up a little lock of my hair with a thread to keep witches away. But I hadn't no confidence. You do that when you've lost a horseshoe that you've found, instead of nailing it up over the ");
  Keyboard.print("door, but I hadn't ever heard anybody say it was any way to keep off bad luck when you'd killed a spider. I set down again, a-shaking all over, and got out my pipe for a smoke; for the house was all as still as death now, and so the widow wouldn't know. Well, ");
  Keyboard.print("after a long time I heard the clock away off in the town go boom -- boom -- boom -- twelve licks; and all still again -- stiller than ever. Pretty soon I heard a twig snap down in the dark amongst the trees -- something was a stirring. I set still and ");
  Keyboard.print("listened. Directly I could just barely hear a 'me-yow! me- yow!' down there. That was good! Says I, 'me- yow! me-yow!' as soft as I could, and then I put out the light and scrambled out of the window on to the shed. Then I slipped down to the ground and crawled ");
  Keyboard.print("in among the trees, and, sure enough, there was Tom Sawyer waiting for me."); 
  Keyboard.print("We went tiptoeing along a path amongst the trees back towards the end of the widow's garden, stooping down so as the branches wouldn't scrape our heads. When we was passing by the kitchen I fell over a root and made a noise. We scrouched down and laid still. Miss ");
  Keyboard.print("Watson's big nigger, named Jim, was setting in the kitchen door; we could see him pretty clear, because there was a light behind him. He got up and stretched his neck out about a minute, listening. Then he says: He listened some more; then he come tiptoeing down ");
  Keyboard.print("and stood right between us; we could a touched him, nearly. Well, likely it was minutes and minutes that there warn't a sound, and we all there so close together. There was a place on my ankle that got to itching, but I dasn't scratch it; and then my ear begun to ");
  // DO NOT ADD ANY MORE KEYBOARD ENTRIES, the teensy will error out if you do.
}

/*
####################################################################
System functions, dont modify unless you know what your doing
####################################################################
*/
// Used to clean up test code so we don't have to put all of this code for every character we want to run against.
void keyboard_press(int key, int modifier){
  Keyboard.set_modifier(modifier);
  Keyboard.send_now();
  Keyboard.set_key1(key);
  Keyboard.send_now();
  Keyboard.set_modifier(0);
  Keyboard.set_key1(0);
  Keyboard.send_now();
}

// Used to start a process by using the windows key + r to run, then typing in the name passed as an arguement and hit enter
void start_process(String process){
  keyboard_press(KEY_R,MODIFIERKEY_GUI);
  delay(500);
  Keyboard.print(process);
  delay(500);
  keyboard_press(KEY_ENTER,0);
  delay(500);
}

// Used to calculate milisecond delay between keyboard presses based on words per minute input
int word_per_minute(int rate){
  // Average number of characters in english words is 4.5
  // Add the rate to the end to compute the addition of spaces
  int wpm = (rate * 4.5) + rate;
  // Now find characters per minute by deviding words per minute by 60
  int cpm = wpm / 60;
  // Finially devide 1000 by cpm to produce number of miliseconds to delay between key presses
  return 1000 / cpm;
} 

/* 
Move the mouse to the upper left corner to start. 
Use 1920 so that it SHOULD work on any resolution monitor
Positive X moves to the right. Positive Y moves downwards
*/
void mouse_reset(){
  int i=0;
  for (i=0; i<1920; i++) {
    Mouse.move(-2, -2);
    delay(1);
  }
}

/* 
Move the mouse to the specified x and y coordinates on the screen. 
*/
void mouse_move(int x, int y){
  int i=0;
  // Had to multiply in order to get to exact location on screen.
  for (i=0; i<(x*.76); i++) {
    Mouse.move(2, 0);
    delay(1);
  }
  i=0;
  // Had to multiply in order to get to exact location on screen.
  for (i=0; i<(y*.77); i++) {
    Mouse.move(0, 2);
    delay(1);
  }
}

/*
Start functions used to show specific characters on 7 segment display.
It should take 1 second for 2 rotations.
*/
void display_idle(int seconds){
  int count = 0;
  do {
   //Draws a continues circle around the display
   char_TOP();
   delay(125);
   char_RIGHT();
   delay(125);
   char_BOTTOM();
   delay(125);
   char_LEFT();
   delay(125);
   char_TOP();
   delay(125);
   char_RIGHT();
   delay(125);
   char_BOTTOM();
   delay(125);
   char_LEFT();
   delay(125);
   count = count + 1;
  } while (count < int(seconds));
  char_null();
}

void display_countdown(){
  char_9();
  delay(1000);
  char_8();
  delay(1000);
  char_7();
  delay(1000);
  char_6();
  delay(1000);
  char_5();
  delay(1000);
  char_4();
  delay(1000);
  char_3();
  delay(1000);
  char_2();
  delay(1000);
  char_1();
  delay(1000);
  char_0();
  delay(1000);
}

/*
Define which pins to turn on to print the coresponding character
1 = On
0 = Off
*/
void char_TOP()
{
  digitalWrite(PIN_E7, 0);
  digitalWrite(PIN_E6, 1);
  digitalWrite(PIN_D7, 1);
  digitalWrite(PIN_D5, 1);
  digitalWrite(PIN_D4, 1);
  digitalWrite(PIN_B0, 1);
  digitalWrite(PIN_B1, 1);
}
void char_RIGHT()
{
  digitalWrite(PIN_E7, 1);
  digitalWrite(PIN_E6, 0);
  digitalWrite(PIN_D7, 0);
  digitalWrite(PIN_D5, 1);
  digitalWrite(PIN_D4, 1);
  digitalWrite(PIN_B0, 1);
  digitalWrite(PIN_B1, 1);
}
void char_BOTTOM()
{
  digitalWrite(PIN_E7, 1);
  digitalWrite(PIN_E6, 1);
  digitalWrite(PIN_D7, 1);
  digitalWrite(PIN_D5, 0);
  digitalWrite(PIN_D4, 1);
  digitalWrite(PIN_B0, 1);
  digitalWrite(PIN_B1, 1);
}
void char_LEFT()
{
  digitalWrite(PIN_E7, 1);
  digitalWrite(PIN_E6, 1);
  digitalWrite(PIN_D7, 1);
  digitalWrite(PIN_D5, 1);
  digitalWrite(PIN_D4, 0);
  digitalWrite(PIN_B0, 0);
  digitalWrite(PIN_B1, 1);
}
void char_9()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 1);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 0);
}
void char_8()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 0);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 0);
}
void char_7()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 1);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 1);
 digitalWrite(PIN_B1, 1);
}
void char_6()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 1);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 0);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 0);
}
void char_5()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 1);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 0);
}
void char_4()
{
 digitalWrite(PIN_E7, 1);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 1);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 0);
}
void char_3()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 1);
 digitalWrite(PIN_B1, 0);
}
void char_2()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 1);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 0);
 digitalWrite(PIN_B0, 1);
 digitalWrite(PIN_B1, 0);
}
void char_1()
{
 digitalWrite(PIN_E7, 1);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 1);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 1);
 digitalWrite(PIN_B1, 1);
}
void char_0()
{
 digitalWrite(PIN_E7, 0);
 digitalWrite(PIN_E6, 0);
 digitalWrite(PIN_D7, 0);
 digitalWrite(PIN_D5, 0);
 digitalWrite(PIN_D4, 0);
 digitalWrite(PIN_B0, 0);
 digitalWrite(PIN_B1, 1);
}
void char_null()
{
 digitalWrite(PIN_E7, 1);
 digitalWrite(PIN_E6, 1);
 digitalWrite(PIN_D7, 1);
 digitalWrite(PIN_D5, 1);
 digitalWrite(PIN_D4, 1);
 digitalWrite(PIN_B0, 1);
 digitalWrite(PIN_B1, 1);
}
