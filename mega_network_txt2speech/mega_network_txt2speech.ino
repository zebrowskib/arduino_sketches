#include <Ethernet.h>
#include <SPI.h>
#include <SoftwareSerial.h>

// This is used for the ethernet shield.
boolean reading = false;

// Setup connections for txt 2 speech module to work.
#define rxPin A8                                            // Serial input (connects to Emic 2 SOUT)
#define txPin A9                                            // Serial output (connects to Emic 2 SIN)
SoftwareSerial emicSerial = SoftwareSerial(rxPin, txPin);  // Set up a new serial port

// Setup the ethernet shield (NOTE: You MUST change this MAC address for EACH shield)
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0x9C, 0x3B };  // MAC address from Ethernet shield sticker under board
IPAddress ip(192,168,0,25);                           // IP address
IPAddress gateway(192,168,0,1);                       // Gateway address
IPAddress subnet(255,255,255,0);                      // Subnet mask
EthernetServer server = EthernetServer(80);           //port 80
                       

void setup(){
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  emicSerial.begin(9600);
  emicSerial.print('\n');              // Send a CR in case the system is already up
  while (emicSerial.read() != ':');    // When the Emic 2 has initialized and is ready, it will send a single ':' 
                                       // character, so wait here until we receive it
  delay(10);                           // Short delay
  emicSerial.flush();                  // Flush the receiver buffer
  emicSerial.print("V18\n");           // Set the volume (range from -48 to 18)
  while (emicSerial.read() != ':');
  emicSerial.print("N1\n");            // Set the voice (range from 0 to 8)
  while (emicSerial.read() != ':');
  emicSerial.print("L0\n");            // Set the language (0=english, 1=castilian spanish, 2=latin spanish)
  while (emicSerial.read() != ':');
  emicSerial.print("W230\n");          // Set the speech rate (range from 75 to 600)
  while (emicSerial.read() != ':');
  
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
}

void loop(){
  EthernetClient client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean sentHeader = false;

    while (client.connected()) {
      if (client.available()) {
        if(!sentHeader){
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          // send web page
          client.println("<!DOCTYPE html>");
          client.println("<html>");
          client.println("  <head>");
          client.println("    <style type=\"text/css\">");
          client.println("      body {background-color:black;color:white;}");
          client.println("      p {color:blue;}");
          client.println("    </style>");
          client.println("    <title>Arduino Test</title>");
          client.println("  </head>");
          client.println("  <body>");
          client.println("    Please note that for mouse movements it is REQUIRED that the following criteria be met:");
          client.println("    <ul>");
          client.println("      <li>Screen resolution should be set to 1024 x 768.</li>");
          client.println("      <li>Modify the following reg keys to disable mouse acceleration per user profile.</li>");
          client.println("      <ul>");
          client.println("        <li>HKCU/Control Panel/Mouse/");
          client.println("        <ul>");
          client.println("          <li>MouseThreshold1 = 0</li>");
          client.println("          <li>MouseThreshold2 = 0</li>");
          client.println("          <li>MouseSpeed = 0</li>");
          client.println("        </ul>");
          client.println("      </ul>");
          client.println("    </ul>");
          client.println("    Available tests:<br>");
          // Start listing the tests you want to run, single letter/number ONLY. This letter/number
          // MUST match in the below IF statement if you want the test to actually run.
          client.println("    <button onclick=\"location.href='?0'\">Speech test</button><br>");
          client.println("    <hr>");
          client.println("    The current test is: ");
          sentHeader = true;
        }

        char c = client.read();

        if(reading && c == ' ') reading = false;
        if(c == '?') reading = true; //found the ?, begin reading the info

        if(reading){
          /*
          This is where we define which test we want to run based 
          on the ID number passed in the url. If you want the test
          to print out text to the user to let them know the test
          is running, you MUST include "client" as an option as the
          last arguement and include that in your function.
          */
          if (c == '0')
            test_speech("Testing the text to speech module on the arduino.",client);
        }
        
        if (c == '\n' && currentLineIsBlank)  break;
        if (c == '\n') {
          currentLineIsBlank = true;
        }else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(5); // give the web browser time to receive the data
    client.stop(); // close the connection:
  } 
}

/* 
####################################################################
Start Defining what tests you want to perform (no particular order)
####################################################################
*/
void test_speech(String txt2say, EthernetClient client) {
  client.println("Speech function test in progress.<br>You should hear the following text played: ");
  client.println(txt2say);
  txt2speech(txt2say);
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">"); 
}

/*
####################################################################
System functions, dont modify unless you know what your doing
####################################################################
*/
// Used for the txt to speech module
void txt2speech(String txt2say){
  emicSerial.print('S');
  emicSerial.print(txt2say);
  emicSerial.print('\n');
  while (emicSerial.read() != ':');
}

