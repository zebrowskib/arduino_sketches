/*
Origional Idea: 
http://learn.parallax.com/contest/clear-table

Reference for continues rotation servos
180  =  Counter-Clockwise
0  =  Clockwise
90  =  Stationary
Anything in between = speed

Here are the connections necessary:
servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10
eyes servo-------------------D12

ping sensor------------------Arduino
------------------------------------
Control pin------------------A0
+5v--------------------------A1
grnd-------------------------A2

xbee module-----------------Arduino
-----------------------------------
+5v-------------------------2
grnd------------------------3

qti sensor-------------------Arduino
------------------------------------
grnd-------------------------5
control pin------------------6
+5v--------------------------7
NOTE: Red wire on QTI sensor is data, NOT +5v!!!
*/
// Include libraries for Servo
#include <Servo.h>

//Define the range that the target should be away from your bot.
//Define the minimum distance the target can be
#define minDistance 1
//Define the maximum distance the target can be
#define maxDistance 24

//Define the range that the color for the arena will be
//Define the minimum color for the playing field
#define minColor 100
//Define the maximum color for the playing field
#define maxColor 1200

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
#define eyes_servo_pin 12
Servo servoRight;
Servo servoLeft;
Servo servoEyes;

// Begin configuration for QTI sensor
//#define qtiGndPin 5
//#define qti5vPin 7
//#define qti_pin 6
#define qtiGndPin A5
#define qti5vPin A3
#define qti_pin A4

// Configuration for the ping sensor
#define pingGndPin A2
#define ping5vPin A1
#define pingPin A0
unsigned int duration, inches;

// Configuration for the xbee power
#define xbee5vPin 2
#define xbeeGndPin 3
boolean fightStatus = false;

void setup()
{
  Serial.begin(9600);
  
  // Setup the servos
  servoRight.attach(right_servo_pin, 0, 180); 
  servoLeft.attach(left_servo_pin, 0, 180); 
  servoEyes.attach(eyes_servo_pin, 0, 180); 
  servoEyes.write(90);
  
  // Set the pins to be used for the qti sensor for power
  pinMode(qti5vPin, OUTPUT);
  pinMode(qtiGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(qti5vPin, HIGH);
  digitalWrite(qtiGndPin, LOW);
  
  // Set the pins to be used for the ping sensor for power
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);

  // Set the pins to be used for the xbee module for power
  pinMode(xbee5vPin, OUTPUT);
  pinMode(xbeeGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(xbee5vPin, HIGH);
  digitalWrite(xbeeGndPin, LOW);

  delay(3000); // pause for a bit for the xbee to power up and start talking
  Serial.println("Servos: Done with setup, waiting for command to begin.");
}
void loop()
{
  if (Serial.find("fight")) {
    fightStatus = true;
    Serial.println("Servos: Time to Fight!");
  }
  if (fightStatus == true) {
    int cur_distance = ping_distance();
    Serial.print("Servos: Current distance: ");
    Serial.println(String(cur_distance));
    if ((cur_distance > minDistance) && (cur_distance < maxDistance)){
      // Delay half a second to try to get the target more directly in front of the plow
      Serial.println("Servos: Pushing target");
      pushTarget();
      Serial.println("Servos: Backing up");
      backup();
      // Turn a little just in case the target didn't fall
      servoRight.write(180);
      servoLeft.write(180);  
      delay(500);
    }
    else{
      servoRight.write(180);
      servoLeft.write(180);
    }
    delay(50);
    if (Serial.find("stop")) {
      fightStatus = false;
      Serial.println("Servos: Stand down.");
    }
  }
}

// Find the distance using the ping sensor.
int ping_distance(){
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              // with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  return inches;                     // Return the results
  //If you want cm, comment out above line and uncomment this one
  //return (inches*2.54);
}

// Backup the robot so we can go again.
void backup() {
  // Stop all motors
  servoRight.write(90);
  servoLeft.write(90);
  // Backup
  servoRight.write(180);
  servoLeft.write(0);  
  delay(500);
  // Stop all motors
  servoRight.write(90);
  servoLeft.write(90);
}

// Push the target until the qti sensor finds the edge of the arena
void pushTarget(){
  // Stop all motors
  servoRight.write(90);
  servoLeft.write(90);
  Serial.print("Servos: QTI Reading: ");
  Serial.println(String(RCTime(qti_pin)));
  // Go forward so long as the qti sensor doesn't read the edge
  while ((RCTime(qti_pin) > minColor) && (RCTime(qti_pin) < maxColor)) {  
    Serial.print("Servos: QTI Reading: ");
    Serial.println(String(RCTime(qti_pin)));
    // Move forward
    servoRight.write(0);
    servoLeft.write(180);
  }
  // Stop all motors
  servoRight.write(90);
  servoLeft.write(90);
}

// Find time for light to travel back to qti sensor
long RCTime(int sensorIn){
   long duration = 0;
   pinMode(sensorIn, OUTPUT);     // Make pin OUTPUT
   digitalWrite(sensorIn, HIGH);  // Pin HIGH (discharge capacitor)
   delay(1);                      // Wait 1ms
   pinMode(sensorIn, INPUT);      // Make pin INPUT
   digitalWrite(sensorIn, LOW);   // Turn off internal pullups
   while(digitalRead(sensorIn)){  // Wait for pin to go LOW
      duration++;
   }
   return duration;
}
