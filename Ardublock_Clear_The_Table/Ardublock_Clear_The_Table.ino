#include <Servo.h>

int _ABVAR_1_Distance;
Servo servo_pin_11;
Servo servo_pin_10;
int ardublockUltrasonicSensorCodeAutoGeneratedReturnCM(int trigPin, int echoPin)
{
  int duration;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(20);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  duration = duration / 59;
  return duration;
}


void setup()
{
servo_pin_11.attach(11);
pinMode( 15 , OUTPUT);
servo_pin_10.attach(10);
_ABVAR_1_Distance = 0;
digitalWrite( 14 , LOW );

pinMode( 7 , OUTPUT);
pinMode( 16 , OUTPUT);
pinMode( 5 , OUTPUT);
pinMode( 6 , INPUT);
}

void loop()
{
digitalWrite( 7 , HIGH );
digitalWrite( 5 , LOW );
digitalWrite( 15 , HIGH );
digitalWrite( 16 , LOW );
_ABVAR_1_Distance = ardublockUltrasonicSensorCodeAutoGeneratedReturnCM( 14 , 14 ) ;
if (( ( ( _ABVAR_1_Distance ) > ( 1 ) ) && ( ( _ABVAR_1_Distance ) < ( 22 ) ) ))
{
while ( digitalRead( 6) )
{
servo_pin_10.write( 1 );
servo_pin_11.write( 180 );
}

servo_pin_10.write( 180 );
servo_pin_11.write( 1 );
delay( 1000 );
}
else
{
servo_pin_10.write( 180 );
servo_pin_11.write( 180 );
}
delay( 200 );
}


