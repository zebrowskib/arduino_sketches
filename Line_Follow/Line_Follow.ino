/*
Reference for continues rotation servos
1700  =  Counter-Clockwise
1300  =  Clockwise
1500  =  Stationary
Anything in between = speed

servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10

QTI Sensor-------------------D12
*/

// Include libraries for Servo
#include <Servo.h>

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
Servo servoRight;
Servo servoLeft;

// Begin configuration for QTI sensor
#define qti_sensor_1_pin 12

void setup()
{
  // Setup serial output for debugging
  Serial.begin(9600);                   // Begin the serial monitor at 9600bps
  
  // Setup two servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
}  
 
void loop()
{
  Serial.println(RCTime(qti_sensor_1_pin));	    // Connect to pin 12, display results
  if ((RCTime(qti_sensor_1_pin)) > 100)
    go_forward(100);
  else {
    turn_right(100);
      if ((RCTime(qti_sensor_1_pin)) > 100)
        go_forward(100);
      else {
        turn_left(150);
        go_forward(100);
      }
  }
}

long RCTime(int sensorIn){
   long duration = 0;
   pinMode(sensorIn, OUTPUT);     // Make pin OUTPUT
   digitalWrite(sensorIn, HIGH);  // Pin HIGH (discharge capacitor)
   delay(1);                      // Wait 1ms
   pinMode(sensorIn, INPUT);      // Make pin INPUT
   digitalWrite(sensorIn, LOW);   // Turn off internal pullups
   while(digitalRead(sensorIn)){  // Wait for pin to go LOW
      duration++;
   }
   return duration;
}

void go_forward(int distance){
  servoRight.writeMicroseconds(1300);
  servoLeft.writeMicroseconds(1700);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void go_backward(int distance){
  servoRight.writeMicroseconds(1700);
  servoLeft.writeMicroseconds(1300);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void turn_right(int distance){
  servoRight.writeMicroseconds(1700);
  servoLeft.writeMicroseconds(1700);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void turn_left(int degree){
  servoRight.writeMicroseconds(1300);
  servoLeft.writeMicroseconds(1300);
  delay(degree);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}
