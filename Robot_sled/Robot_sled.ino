/*
Reference for continues rotation servos
1700  =  Counter-Clockwise
1300  =  Clockwise
1500  =  Stationary
Anything in between = speed

Turning degrees seems to be close to 600 delay = 90 degree turn.

Here are the connections necessary:
Bluetooth Mate---------------Arduino
------------------------------------
VCC--------------------------3.3V
GND--------------------------GND
TX-O-------------------------D3
RX-I-------------------------D2

Compas-----------------------Arduino
GND--------------------------GND
VIN--------------------------3.3V
SCL--------------------------A5
SDA--------------------------A4

servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10



*/
// Include libraries for Servo
#include <Servo.h>

// Include libraries for bluetooth
#include <SoftwareSerial.h>

// Include libraries for compass
#include <Wire.h>
#include <HMC5883L.h>

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
Servo servoRight;
Servo servoLeft;

// Begin configuration for bluetooth
#define bluetoothRx 2  // RX-I pin of bluetooth mate, Arduino D2
#define bluetoothTx 3  // TX-O pin of bluetooth mate, Arduino D3

SoftwareSerial bluetoothSerial(bluetoothTx, bluetoothRx);
 
// Begin configuration for compass
HMC5883L compass;

int error = 0;

void setup()
{
  // Setup serial output for debugging
  Serial.begin(9600);                   // Begin the serial monitor at 9600bps
  
  // Setup two servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
  
  // Setup bluetooth module
  Serial.println("Starting...");  
  bluetoothSerial.begin(115200);        // The Bluetooth Mate defaults to 115200bps
  delay(5000);                          // IMPORTANT DELAY! (Minimum ~276ms)
  bluetoothSerial.print("$$$");         // Enter command mode
  delay(5000);                          // IMPORTANT DELAY! (Minimum ~10ms)
  bluetoothSerial.println("SN,Robot");
  delay(500);
  bluetoothSerial.println("U,9600,N");  // Temporarily Change the baudrate to 9600, no parity
  bluetoothSerial.begin(9600);          // Start bluetooth serial at 9600 
  
  Wire.begin();                         // Start the I2C interface.
  compass = HMC5883L();                 // Construct a new HMC5883 compass.
  error = compass.SetScale(1.3);        // Set the scale of the compass.
  error = compass.SetMeasurementMode(Measurement_Continuous); // Set the measurement mode to Continuous
}  
 
void loop()
{
  //bluetoothSerial.println(get_compass_reading());
  //Serial.println(get_compass_reading());
  if(bluetoothSerial.available()) { 
    switch (bluetoothSerial.read()) {
      case 'w':
        go_forward(1000);
        Serial.println("forward");
        bluetoothSerial.println("forward");
        break;
      case 'a':
        turn_left(300);
        Serial.println("left");
        bluetoothSerial.println("left");
        break;
      case 's':
        go_backward(1000);
        Serial.println("backward");
        bluetoothSerial.println("backward");
        break;
      case 'd':
        turn_right();
        Serial.println("right");
        bluetoothSerial.println(get_compass_reading());
        break;
      default: 
        // if nothing else matches, do the default
        Serial.print((char)bluetoothSerial.read()); 
        bluetoothSerial.print((char)bluetoothSerial.read());
        break;
    }
    
  }
}

void go_forward(int distance){
  servoRight.writeMicroseconds(1300);
  servoLeft.writeMicroseconds(1700);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void go_backward(int distance){
  servoRight.writeMicroseconds(1700);
  servoLeft.writeMicroseconds(1300);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void turn_right(){
  int initial_heading = get_compass_reading();
  int temp_heading = initial_heading + 45;
  int end_heading;
  
  if (temp_heading > 360)
    end_heading = temp_heading - 360;
  else
    end_heading = temp_heading;
  bluetoothSerial.println("Initial Heading:");
  bluetoothSerial.println(initial_heading);
  bluetoothSerial.println("Ending Heading:");
  bluetoothSerial.println(end_heading);
  int stop_value = 0;
  while (stop_value = 0) {
    if ((get_compass_reading() > (end_heading-2)) && (get_compass_reading() < (end_heading+2))) {
      stop_value = 1;
      continue;
    }
    else {
      servoRight.writeMicroseconds(1700);
      servoLeft.writeMicroseconds(1700);
      delay(1);
      servoRight.writeMicroseconds(1500);
      servoLeft.writeMicroseconds(1500);
    }
  }
}

void turn_left(int degree){
  servoRight.writeMicroseconds(1300);
  servoLeft.writeMicroseconds(1300);
  delay(degree);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

int get_compass_reading(){
  MagnetometerScaled scaled = compass.ReadScaledAxis();

  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float heading = atan2(scaled.YAxis, scaled.XAxis);
  
  // Correct for when signs are reversed.
  if(heading < 0)
    heading += 2*PI;
    
  // Check for wrap due to addition of declination.
  if(heading > 2*PI)
    heading -= 2*PI;
   
  // Convert radians to degrees for readability.
  int headingDegrees = heading * 180/M_PI; 
  return headingDegrees;
}
