// GPS Setup:
// VIN - 5V
// GND - GND
// RX - D2
// TX - D3
#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(3, 2);
Adafruit_GPS GPS(&mySerial);
// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO  false
// this keeps track of whether we're using the interrupt
// off by default!
boolean usingInterrupt = false;
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy

#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 11, 10, 8, 7, 6);

#include <PWMServo.h> 
PWMServo myservo;
int pos = 89;

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 500;    // the debounce time; increase if the output flickers
int lastButtonState = LOW;   // the previous reading from the input pin
const int button1pin = 14;

int i;

/*************************************************************************
 * User defined variables
 *************************************************************************/
//Define how close you must be from your target to move to next
int distance = 5;
//Input your lat and lon locations one at a time.
//int coordArray[][3] = {
//                      {35.163549, 35.164066, 35.163555},
//                      {78.994328, 78.995651, 78.994340}
//                      };
float latArray[] = {35.163549, 35.164066, 35.163555};
float lonArray[] = {78.994328, 78.995651, 78.994340};

//float dest_lat = 35.163549;
//float dest_lon = 78.994328;
/*************************************************************************
 * End user defined variables
 *************************************************************************/

void setup()  
{    
  // connect at 115200 so we can read the GPS fast enough and echo without dropping chars
  // also spit it out
  Serial.begin(115200);

  // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
  GPS.begin(9600);
  
  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  // uncomment this line to turn on only the "minimum recommended" data
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
  // the parser doesn't care about other sentences at this time
  
  // Set the update rate
  // 1 Hz
  //GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  // 5 Hz, RMC or RMCGGA ONLY!
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
  // 10 Hz, RMC ONLY!
  //GPS.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);

  // the nice thing about this code is you can have a timer0 interrupt go off
  // every 1 millisecond, and read data from the GPS for you. that makes the
  // loop code a heck of a lot easier!
  //useInterrupt(true);
  
  lcd.begin(16,2);
  
  myservo.attach(9);
  myservo.write(89);
    
  pinMode(button1pin, INPUT);
}

// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO)
    if (c) UDR0 = c;  
    // writing direct to UDR0 is much much faster than Serial.print 
    // but only one character can be written at a time. 
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }
}

void loop()
{  
  for (i=0;i<(sizeof(latArray)/sizeof(latArray[0]));) {
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }
    //Dont bother writing anything until we get a valid GPS signal
    if (GPS.fix == 1) {
      float current_distance = calc_dist(latArray[i],lonArray[i],convertDegMinToDecDeg(GPS.latitude),convertDegMinToDecDeg(GPS.longitude));
      lcd.setCursor(0, 0);
      lcd.print("Distance:");
      lcd.setCursor(0, 1);
      lcd.print(current_distance,0);
      lcd.setCursor(4, 1);
      lcd.print("meters");
      //if you are within the specified range of your target, increment by one till you reach the end, then unlock.
      if (current_distance < distance) myservo.write(0);
      //i++;
      //pause for 5 seconds
      delay(2000);
      lcd.clear();
    }
    else {
      lcd.setCursor(0, 0);
      lcd.print("No GPS signal.");
      lcd.setCursor(0,1);
      lcd.print("Please wait.");
      delay(2000);
      lcd.clear();
    }

    int button1state = digitalRead(button1pin);
    //if(button1state == HIGH) {
    if((millis() - lastDebounceTime) > debounceDelay){
      if(button1state == HIGH){
        myservo.write(0); //Unlock the box
        if((millis() - lastDebounceTime) > debounceDelay) i++;
        button1state == LOW;
        //delay(1000);
      }
      else {
        myservo.write(89); //Ensure the box stays locked
      }
      lastDebounceTime = millis(); 
    }
    /*************************************************************************
     * Debug section
     *************************************************************************/
     Serial.print(i);Serial.print("; "); Serial.print(latArray[i],6); Serial.print("; "); Serial.print(lonArray[i],6); Serial.print("; "); Serial.print('\n');
  }
}
/*************************************************************************
 * //Function to convert ddmm.mmmm to dd.dddddd
 *************************************************************************/
double convertDegMinToDecDeg (float degMin) {
  double min = 0.0;
  double decDeg = 0.0;
 
  //get the minutes, fmod() requires double
  min = fmod((double)degMin, 100.0);
 
  //rebuild coordinates in decimal degrees
  degMin = (int) ( degMin / 100 );
  decDeg = degMin + ( min / 60 );
 
  return decDeg;
}

/*************************************************************************
 * //Function to calculate the distance between two waypoints
 *************************************************************************/
float calc_dist(float flat1, float flon1, float flat2, float flon2)
{
  float dist_calc=0;
  float dist_calc2=0;
  float diflat=0;
  float diflon=0;
  
  //I've to spplit all the calculation in several steps. 
  //If i try to do it in a single line the arduino will explode.
  diflat=radians(flat2-flat1);
  flat1=radians(flat1);
  flat2=radians(flat2);
  diflon=radians((flon2)-(flon1));
  
  dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
  dist_calc2= cos(flat1);
  dist_calc2*=cos(flat2);
  dist_calc2*=sin(diflon/2.0);
  dist_calc2*=sin(diflon/2.0);
  dist_calc +=dist_calc2;
  
  dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
  
  dist_calc*=6371000.0; //Converting to meters
  //Serial.println(dist_calc);
  return dist_calc;
}

/*************************************************************************
 * //Function to calculate the bearing between two waypoints
 *************************************************************************/
float calc_bearing(float flat1, float flon1, float flat2, float flon2)
{
  float heading = 0;
  flon1 = radians(flon1);  //also must be done in radians
  flon2 = radians(flon2);  //radians duh.
  //heading = atan2(sin(flon2-flon1)*cos(flat2),cos(flat1)*sin(flat2)-sin(flat1)*cos(flat2)*cos(flon2-flon1)),2*3.1415926535;
  
  heading = atan2(cos(flat1)*sin(flat2)-sin(flat1)*cos(flat2)*cos(flon2-flon1),sin(flon2-flon1)*cos(flat2));
  heading = heading*180/3.1415926535;  // convert from radians to degrees
  int head =heading; //make it a integer now
  if(head<0){
     heading+=360;   //if the heading is negative then add 360 to make it positive
  }
  return heading;
}

/*************************************************************************
 * //Function to unlock the box by moving the server to 90 degrees
 *************************************************************************/
void unlock()
{
  //myservo1.write(90);
}
