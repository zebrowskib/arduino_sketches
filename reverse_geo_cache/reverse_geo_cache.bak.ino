/* 
servo positive goes to 5V, negative to grd and control to d5
*/
#include <PWMServo.h> 
#include <LiquidCrystal.h>

/* 
GPS vin goes to 5v, gnd to gnd, rx to d2, tx to d3
*/
#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(3, 2);
Adafruit_GPS GPS(&mySerial);

PWMServo myservo1;

// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO  false;

// this keeps track of whether we're using the interrupt
// off by default!
boolean usingInterrupt = false;
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy

LiquidCrystal lcd(12, 11, 10, 8, 7, 6);

int button1 = 14;
int button1state = 0;

float dest_lat = 35.164128;
float dest_lon = -78.995723;
float test_lat = 35.163749;
float test_lon = -78.994773;

void setup()
{
  // connect at 115200 so we can read the GPS fast enough and echo without dropping chars
  // also spit it out
  Serial.begin(115200);
  
  // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
  GPS.begin(9600);
  
  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  
  // Set the update rate
  // 1 Hz
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  
  // the nice thing about this code is you can have a timer0 interrupt go off
  // every 1 millisecond, and read data from the GPS for you. that makes the
  // loop code a heck of a lot easier!
  useInterrupt(true);
  
  myservo1.attach(9); //attach control pin of servo to digital 9
  myservo1.write(89);
  
  lcd.begin(16,2);
  
  pinMode(button1, INPUT);
  
}

void loop()
{
  // if a sentence is received, we can check the checksum, parse it...
  if (GPS.newNMEAreceived()) {
    // a tricky thing here is if we print the NMEA sentence, or data
    // we end up not listening and catching other sentences! 
    // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
    //Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
  
    if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }
  //Dont bother writing anything until we get a valid GPS signal
  if (GPS.altitude > 0) {
    
    lcd.setCursor(0, 0);
    lcd.print("Distance:");
    lcd.setCursor(0, 1);
    
    //calc_dist(GPS.latitude,GPS.longitude,dest_lat,dest_lon)
    lcd.print(calc_dist(dest_lat,dest_lon,GPS.latitude,GPS.longitude));
    lcd.setCursor(6, 1);
    lcd.print("meters");
    //pause for 5 seconds
    delay(5000);
    lcd.clear();
    button1state = digitalRead(button1);
  }
  //if (button1state == HIGH) {
  //  myservo1.write(0); }  //Unlock the box
  //else if (button1state == LOW) {
  //  myservo1.write(89); } //Ensure the box stays locked
}

/*************************************************************************
 * //Function to calculate the distance between two waypoints
 *************************************************************************/
float calc_dist(float flat1, float flon1, float flat2, float flon2)
{
  float dist_calc=0;
  float dist_calc2=0;
  float diflat=0;
  float diflon=0;
  
  //I've to spplit all the calculation in several steps. 
  //If i try to do it in a single line the arduino will explode.
  diflat=radians(flat2-flat1);
  flat1=radians(flat1);
  flat2=radians(flat2);
  diflon=radians((flon2)-(flon1));
  
  dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
  dist_calc2= cos(flat1);
  dist_calc2*=cos(flat2);
  dist_calc2*=sin(diflon/2.0);
  dist_calc2*=sin(diflon/2.0);
  dist_calc +=dist_calc2;
  
  dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
  
  dist_calc*=6371000.0; //Converting to meters
  //Serial.println(dist_calc);
  return dist_calc;
}

/*************************************************************************
 * //Function to calculate the bearing between two waypoints
 *************************************************************************/
float calc_bearing(float flat1, float flon1, float flat2, float flon2)
{
  float heading = 0;
  //flon1 = radians(flon1);  //also must be done in radians
  //flon2 = radians(flon2);  //radians duh.
  //heading = atan2(sin(flon2-flon1)*cos(flat2),cos(flat1)*sin(flat2)-sin(flat1)*cos(flat2)*cos(flon2-flon1)),2*3.1415926535;
  
  heading = atan2(cos(flat1)*sin(flat2)-sin(flat1)*cos(flat2)*cos(flon2-flon1),sin(flon2-flon1)*cos(flat2));
  //heading = heading*180/3.1415926535;  // convert from radians to degrees
  int head =heading; //make it a integer now
  if(head<0){
     heading+=360;   //if the heading is negative then add 360 to make it positive
  }
  return heading;
}

/*************************************************************************
 * //Function to unlock the box by moving the server to 90 degrees
 *************************************************************************/
void unlock()
{
  myservo1.write(90);
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }
}
