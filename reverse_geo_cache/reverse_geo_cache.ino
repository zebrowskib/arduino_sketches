// GPS Setup:
// VIN - 5V
// GND - GND
// RX - D2
// TX - D3
#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
SoftwareSerial mySerial(40,42);
Adafruit_GPS GPS(&mySerial);
// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO  true

// this keeps track of whether we're using the interrupt
// off by default!
boolean usingInterrupt = false;
void useInterrupt(boolean); // Func prototype keeps Arduino 0023 happy

#include <LiquidCrystal.h>
LiquidCrystal lcd(26, 28, 30, 32, 34, 36);

// Include libraries for Servo
#include <Servo.h>
#define servo_pin 46
Servo servoLatch;

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 500;    // the debounce time; increase if the output flickers
int lastButtonState = LOW;   // the previous reading from the input pin
const int button1pin = 4;
// Set DEBUG to true in order to enable serial ouput of values.
// !!!WARNING!!! Don't enable DEBUG and GPSECHO at the same time. (For your own sake)
#define DEBUG  false

#include <SPI.h>
#include <SD.h>
#include <IniFile.h>
File myFile;
// change this to match your SD shield or module;
//     Arduino Ethernet shield: pin 4
//     Adafruit SD shields and modules: pin 10
//     Sparkfun SD shield: pin 8
//     Arduino Mega: pin 53
const int SD_SELECT = 53;

int i;

byte degree[8] = { // shape of the degree symbol
B00110,
B01001,
B01001,
B00110,
B00000,
B00000,
B00000,
B00000
};

/*************************************************************************
 * User defined variables
 *************************************************************************/
//Define how close you must be from your target to move to next
int attempts;
int distance;
int totalCoords;
//Input your lat and lon locations one at a time.
//int coordArray[][3] = {
//                      {35.163549, 35.164066, 35.163555},
//                      {78.994328, 78.995651, 78.994340}
//                      };
float latArray[] = {35.163549, 35.164066, 35.163555};
float lonArray[] = {78.994328, 78.995651, 78.994340};
//float latArray[10];
//float lonArray[10];
/*************************************************************************
 * End user defined variables
 *************************************************************************/

void setup()  
{
  // connect at 115200 so we can read the GPS fast enough and echo without dropping chars
  // also spit it out
  Serial.begin(115200);

  // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
  GPS.begin(9600);
  
  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  // uncomment this line to turn on only the "minimum recommended" data
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
  // the parser doesn't care about other sentences at this time
  
  // Set the update rate
  // 1 Hz
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  // 5 Hz, RMC or RMCGGA ONLY!
  //GPS.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);
  // 10 Hz, RMC ONLY!
  //GPS.sendCommand(PMTK_SET_NMEA_UPDATE_5HZ);

  // the nice thing about this code is you can have a timer0 interrupt go off
  // every 1 millisecond, and read data from the GPS for you. that makes the
  // loop code a heck of a lot easier!
  useInterrupt(true);
  
  lcd.begin(16,2);
  lcd.createChar(1, degree); // create degree symbol from the binary
  
  servoLatch.attach(servo_pin);
  servoLatch.write(89);
    
  pinMode(button1pin, INPUT);

  // Check to ensure we can read the SD card
  const size_t bufferLen = 80;
  char buffer[bufferLen];
  float something;

  const char *filename = "/geocache.ini";
  SPI.begin();
  if (!SD.begin(SD_SELECT))
      Serial.println("SD.begin() failed");
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("initialization");
      lcd.setCursor(0, 1);
      lcd.print("of SD failed!");
      // Cannot do anything else
      //while (1);
  
  IniFile ini(filename);
  if (!ini.open()) {
    Serial.print("Ini file ");
    Serial.print(filename);
    Serial.println(" does not exist");
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("ini file does");
    lcd.setCursor(0, 1);
    lcd.print("not exist!");
    // Cannot do anything else
    while (1);
  }
  Serial.println("Ini file exists");

  // Check the file is valid. This can be used to warn if any lines
  // are longer than the buffer.
  if (!ini.validate(buffer, bufferLen)) {
    Serial.print("ini file ");
    Serial.print(ini.getFilename());
    Serial.print(" not valid: ");
    // Cannot do anything else
    while (1);
  }
  
  // Try fetching a key from a section which is not present
  if (ini.getValue("geo_setup", "attempts", buffer, bufferLen)) {
    Serial.print("attempts: ");
    attempts = atof(buffer);
    Serial.println(attempts);
  }
  // Try fetching a key from a section which is not present
  if (ini.getValue("geo_setup", "distance", buffer, bufferLen)) {
    Serial.print("distance: ");
    distance = atof(buffer);
    Serial.println(distance);
  }
  // Try fetching a key from a section which is not present
  if (ini.getValue("geo_setup", "totalCoords", buffer, bufferLen)) {
    Serial.print("totalCoords: ");
    totalCoords = atof(buffer);
    Serial.println(totalCoords);
  }
  
//  if (ini.getValue("geo_coords", "lonArray2", buffer, bufferLen)) {
//    Serial.print("long array: ");
//    Serial.println(String(buffer));
//    lonArray = strtod(float(buffer, " ");
//    //sscanf(buffer, "%f, %f, %f", &lonArray[0], something, &lonArray[1]);
//    Serial.println(lonArray[0],6);
//  }
  
  
}

// Interrupt is called once a millisecond, looks for any new GPS data, and stores it
SIGNAL(TIMER0_COMPA_vect) {
  char c = GPS.read();
  // if you want to debug, this is a good time to do it!
  if (GPSECHO)
    if (c) UDR0 = c;  
    // writing direct to UDR0 is much much faster than Serial.print 
    // but only one character can be written at a time. 
}

void useInterrupt(boolean v) {
  if (v) {
    // Timer0 is already used for millis() - we'll just interrupt somewhere
    // in the middle and call the "Compare A" function above
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    usingInterrupt = true;
  } else {
    // do not call the interrupt function COMPA anymore
    TIMSK0 &= ~_BV(OCIE0A);
    usingInterrupt = false;
  }
}

void loop()
{  
  // If we couldn't read the SD card earlier, bail and don't continue
  for (i=0;i<(sizeof(latArray)/sizeof(latArray[0]));) {
    //if (i = (sizeof(latArray)/sizeof(latArray[0]))) i = 0; myservo.write(0);
    // if a sentence is received, we can check the checksum, parse it...
    if (GPS.newNMEAreceived()) {
      // a tricky thing here is if we print the NMEA sentence, or data
      // we end up not listening and catching other sentences! 
      // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
      Serial.println(GPS.lastNMEA());   // this also sets the newNMEAreceived() flag to false
    
      if (!GPS.parse(GPS.lastNMEA()))   // this also sets the newNMEAreceived() flag to false
        return;  // we can fail to parse a sentence in which case we should just wait for another
    }
    //Dont bother writing anything until we get a valid GPS signal
    if (GPS.fix == 1) {
      float current_distance = calc_dist(latArray[i],lonArray[i],convertDegMinToDecDeg(GPS.latitude),convertDegMinToDecDeg(GPS.longitude));
      float new_bearing = calc_bearing(convertDegMinToDecDeg(GPS.latitude), convertDegMinToDecDeg(GPS.longitude), latArray[i], lonArray[i]);
      lcd.setCursor(0, 0);
      lcd.print(new_bearing,0);
      if (new_bearing < 100) lcd.setCursor(2, 0);
      else lcd.setCursor(3, 0);
      lcd.write(1); //print degree symbol
      lcd.setCursor(6,0);
      //lcd.print(bearing_direction(new_bearing));
      lcd.setCursor(0, 1);
      lcd.print(current_distance,0);
      if (current_distance < 100) lcd.setCursor(3, 1);
      else lcd.setCursor(4, 1);
      lcd.print("meters");
      //if you are within the specified range of your target, increment by one till you reach the end, then unlock.
      if (current_distance < distance) i++;
      //pause for 2 seconds
      delay(2000);
      lcd.clear();
    }
    else {
      lcd.setCursor(0, 0);
      lcd.print("No GPS signal.");
      lcd.setCursor(0,1);
      lcd.print("Please wait.");     
      delay(2000);
      lcd.clear();
    }
    int button1state = digitalRead(button1pin);
    if(button1state == HIGH){
        servoLatch.write(0); //Unlock the box
        if((millis() - lastDebounceTime) > debounceDelay) i++;
        button1state == LOW;
        delay(1000);
    }
    else {
      servoLatch.write(89); //Ensure the box stays locked
    }
    lastDebounceTime = millis(); 
    /*************************************************************************
     * Debug section
     *************************************************************************/
    if (DEBUG) {
      Serial.print(int(i)); Serial.print("; "); 
      Serial.print(sizeof(latArray)/sizeof(latArray[0])); Serial.print("; "); 
      Serial.print(latArray[i],6); Serial.print("; "); 
      Serial.print(lonArray[i],6); Serial.print("; "); 
      Serial.print(convertDegMinToDecDeg(GPS.latitude)); Serial.print("; "); 
      Serial.print(convertDegMinToDecDeg(GPS.longitude)); Serial.print("; ");
      Serial.print(calc_bearing(35.163937, 78.995157, 35.163870, 78.995634),0);
      Serial.print((char)1);
      //Serial.print(bearing_direction(dtostrf(calc_bearing(35.163937, 78.995157, 35.163870, 78.995634),3,3,3)));
      Serial.print('\n'); // print carriage return to clean up serial output.
    }
  }
}




/*
*Read value from ini config file
 */
char *readini(char *val) {
  File configFile = SD.open("geocache.ini");
  char lineStr[80] = {""};                                                      //string line buffer
  char *result = NULL;                                                          //the pointer to the string for strtok
  byte readByte;                                                                //byte buffer for chachter reading
  char delims[] = "=";                                                          //set the delimiters(just equals for the ini
  int linePos = 0;                                                              //position register for line string
  while (configFile.available()) {                                              // while we are not at the end of the file
    readByte = configFile.read();                                                 // grab a byte
    if (readByte != 10 && readByte != 13 && linePos < 81 && readByte != 59) {   // and while for each line(limited to 80 characters and stop at comments)
      lineStr[linePos] = readByte;                                              // add string to line
      linePos++;                                                                // move to the next character slot
    }
    else {                                                                      // When we get to the end of the line
      result = strtok( lineStr, delims );                                       // grab the first
      if( strcmp (result,val) == 0) {                                           // if we have the requested value
        configFile.close();                                                     // Close the file
        return (strtok( NULL, delims ));                                        // return a pointer to the appropriate string
      }
      for (int i=0;i<80;i++) {                                                  
        lineStr[i] = NULL;                                                      // Reset the linebuffer to nothing
      }
      linePos = 0;                                                              // Reset the linebuffer position
    }
  }
  configFile.close();                                                           // Close the file
  return NULL;                                                                  // If we find nothing.
}


/*************************************************************************
 * //Function to create lat/lng arrays from ini file
 *************************************************************************/



/*************************************************************************
 * //Function to convert ddmm.mmmm to dd.dddddd
 *************************************************************************/
double convertDegMinToDecDeg (float degMin) {
  double min = 0.0;
  double decDeg = 0.0;
 
  //get the minutes, fmod() requires double
  min = fmod((double)degMin, 100.0);
 
  //rebuild coordinates in decimal degrees
  degMin = (int) ( degMin / 100 );
  decDeg = degMin + ( min / 60 );
 
  return decDeg;
}

/*************************************************************************
 * //Function to calculate the distance between two waypoints
 *************************************************************************/
float calc_dist(float flat1, float flon1, float flat2, float flon2)
{
  float dist_calc=0;
  float dist_calc2=0;
  float diflat=0;
  float diflon=0;
  
  //I've to spplit all the calculation in several steps. 
  //If i try to do it in a single line the arduino will explode.
  diflat=radians(flat2-flat1);
  flat1=radians(flat1);
  flat2=radians(flat2);
  diflon=radians((flon2)-(flon1));
  
  dist_calc = (sin(diflat/2.0)*sin(diflat/2.0));
  dist_calc2= cos(flat1);
  dist_calc2*=cos(flat2);
  dist_calc2*=sin(diflon/2.0);
  dist_calc2*=sin(diflon/2.0);
  dist_calc +=dist_calc2;
  
  dist_calc=(2*atan2(sqrt(dist_calc),sqrt(1.0-dist_calc)));
  
  dist_calc*=6371000.0; //Converting to meters
  //Serial.println(dist_calc);
  return dist_calc;
}

/*************************************************************************
 * //Function to calculate the bearing between two waypoints
 *************************************************************************/
float calc_bearing(float startlat, float startlon, float endlat, float endlon)
{
  float heading = 0;
  startlon = radians(startlon);  //also must be done in radians
  endlon = radians(endlon);  //radians duh.
  heading = atan2(cos(startlat)*sin(endlat)-sin(startlat)*cos(endlat)*cos(endlon-startlon),sin(endlon-startlon)*cos(endlat));
  heading = heading*180/3.1415926535;  // convert from radians to degrees
  int head =heading; //make it a integer now
  if(head<0){
     heading+=360;   //if the heading is negative then add 360 to make it positive
  }
  return int(heading);
}

/*************************************************************************
 * //Function to display the N,E,S,W bearing title.
 *************************************************************************/
char *bearing_direction(char bearing[])
{
  if (int(bearing) >= 337 && int(bearing) < 361) bearing="N"; 
  if (int(bearing) >= 0 && int(bearing) < 22) bearing="N";
  if (int(bearing) >= 22 && int(bearing) < 67) bearing="NE";
  if (int(bearing) >= 67 && int(bearing) < 112) bearing="E";
  if (int(bearing) >= 112 && int(bearing) < 157) bearing="SE";
  if (int(bearing) >= 157 && int(bearing) < 202) bearing="S";
  if (int(bearing) >= 202 && int(bearing) < 247) bearing="SW";
  if (int(bearing) >= 247 && int(bearing) < 292) bearing="W";
  if (int(bearing) >= 292 && int(bearing) < 337) bearing="NW";
  return bearing;
}
