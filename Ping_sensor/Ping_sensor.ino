const int pingGndPin = 5;
const int ping5vPin = 6;
const int pingPin = 7;
unsigned int duration, inches;

void setup() {
  Serial.begin(9600);
  // Set the pins to be used for the ping sensor to output for digitalwrite
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);
}

void loop() {
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              //   with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  Serial.println(inches);            // Display result
  delay(200);		             // Short delay
}
