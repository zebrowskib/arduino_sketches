// include the library code:
#include <SoftwareSerial.h>
#include <LiquidCrystal.h>
#include <LCDKeypad.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);

#define rxPin 10  // Serial input (connects to Emic 2 SOUT)
#define txPin 11  // Serial output (connects to Emic 2 SIN)
SoftwareSerial emicSerial = SoftwareSerial(rxPin, txPin); // Set up a new serial port

int adc_key_val[5] ={50, 200, 400, 600, 800 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;
int oldkey=-1;
int test_num=0;

/*
Each item in here should corespond to a test function.
The order is important as the if statement below will corespond to each tests 
displayable text.
*/

char* test_array[] = {"loop test >", // 0
                     "< english test >", // 1
                     "< arabic test >", // 2
                     "< move mouse"}; // 2
                     
// Find the size of the array dynamically
const int test_array_size = (sizeof(test_array) / sizeof(int)-1);

/*
First option is for how many words per minute you want it to type at.
  NOTE: Guiness book of world records states the fasts was around 180 words per minute, 
        this is our baseline
Second option is how many times the character "1" will be typed onto the two notepad 
docs before we are done.
*/
int type_rate=180;
long type_loop=130000;

void setup(){
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  emicSerial.begin(9600);
  emicSerial.print('\n');            // Send a CR in case the system is already up
  while (emicSerial.read() != ':');  // When the Emic 2 has initialized and is ready, it will send a single ':' character, so wait here until we receive it
  delay(10);                         // Short delay
  emicSerial.flush();                // Flush the receiver buffer
  emicSerial.print("V12\n");         // Set the volume to 12
  while (emicSerial.read() != ':');
  
  Keyboard.begin();
  Mouse.begin();
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  lcd.clear();
  txt2speech("Testing the text to speech module on the arduino."); 
}

void loop(){
  adc_key_in = analogRead(0);    // read the value from the sensor 
  key = get_key(adc_key_in);  // convert into key press
  if (key == 0) // right
    if (test_num == test_array_size)
      test_num=test_array_size;
    else
      test_num=test_num++;
  if (key == 3) // left
    if (test_num > 0)
      test_num=test_num--;
      
  // Now if we hit enter, run the specific test. This is where order is important.
  if (key == 4) // enter
    if (test_num == 0) // corresponding to array item 0
      test_loop(type_rate,type_loop);
    else if (test_num == 1) // corresponding to array item 1
      test_english(type_rate);
    else if (test_num == 2)  // corresponding to array item 2
      test_arabic(type_rate);
    else if (test_num == 3) { // corresponding to array item 3
      // Move the mouse to the upper left corner, then the specified position. 
      // You must reset ever time in order to get to your destination accurately.
      mouse_reset();
      mouse_move(900,700); }
  
  lcd.setCursor(0,0);
  lcd.print("Which test?");
  lcd.setCursor(0,1);
  lcd.print(test_array[test_num]);
  delay(250);
  lcd.clear();
}

/* 
####################################################################
Start Defining what tests you want to perform (no particular order)
####################################################################
*/
void test_english(int type_speed){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("English test");
  lcd.setCursor(0, 1);
  lcd.print("in progress");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad so we can type in it.
  start_process("notepad");
  delay(1000);
  
  // Test out number keys first.
  keyboard_press('1',0);
  delay(delay_time);
  keyboard_press('2',0);
  delay(delay_time);
  keyboard_press('3',0);
  delay(delay_time);
  keyboard_press('4',0);
  delay(delay_time);
  keyboard_press('5',0);
  delay(delay_time);
  keyboard_press('6',0);
  delay(delay_time);
  keyboard_press('7',0);
  delay(delay_time);
  keyboard_press('8',0);
  delay(delay_time);
  keyboard_press('9',0);
  delay(delay_time);
  keyboard_press('0',0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Now number keys with shift on
  keyboard_press('1',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('2',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('3',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('4',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('5',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('6',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('7',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('8',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('9',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('0',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);

  // Test out arrow keys just to verify
  keyboard_press(KEY_UP_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_LEFT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_DOWN_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Start typing the keys on the keyboard
  keyboard_press('a',0);
  delay(delay_time);
  keyboard_press('b',0);
  delay(delay_time);
  keyboard_press('c',0);
  delay(delay_time);
  keyboard_press('d',0);
  delay(delay_time);
  keyboard_press('e',0);
  delay(delay_time);
  keyboard_press('f',0);
  delay(delay_time);
  keyboard_press('g',0);
  delay(delay_time);
  keyboard_press('h',0);
  delay(delay_time);
  keyboard_press('i',0);
  delay(delay_time);
  keyboard_press('j',0);
  delay(delay_time);
  keyboard_press('k',0);
  delay(delay_time);
  keyboard_press('l',0);
  delay(delay_time);
  keyboard_press('m',0);
  delay(delay_time);
  keyboard_press('n',0);
  delay(delay_time);
  keyboard_press('o',0);
  delay(delay_time);
  keyboard_press('p',0);
  delay(delay_time);
  keyboard_press('q',0);
  delay(delay_time);
  keyboard_press('r',0);
  delay(delay_time);
  keyboard_press('s',0);
  delay(delay_time);
  keyboard_press('t',0);
  delay(delay_time);
  keyboard_press('u',0);
  delay(delay_time);
  keyboard_press('v',0);
  delay(delay_time);
  keyboard_press('w',0);
  delay(delay_time);
  keyboard_press('x',0);
  delay(delay_time);
  keyboard_press('y',0);
  delay(delay_time);
  keyboard_press('z',0);
  delay(delay_time);
  keyboard_press(0x2D,0); // -
  delay(delay_time);
  keyboard_press(0x3D,0); // =
  delay(delay_time);
  keyboard_press(0x5B,0); // [
  delay(delay_time);
  keyboard_press(0x5D,0); // ]
  delay(delay_time);
  keyboard_press(0x5C,0); // \
  delay(delay_time);
  keyboard_press(0x3B,0); // ;
  delay(delay_time);
  keyboard_press(0x27,0); // '
  delay(delay_time);
  keyboard_press(0x60,0); // `
  delay(delay_time);
  keyboard_press(0x2C,0); // ,
  delay(delay_time);
  keyboard_press(0x2E,0); // .
  delay(delay_time);
  keyboard_press(0x2F,0); // /
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Same thing as above, but with caps lock on
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press('a',0);
  delay(delay_time);
  keyboard_press('b',0);
  delay(delay_time);
  keyboard_press('c',0);
  delay(delay_time);
  keyboard_press('d',0);
  delay(delay_time);
  keyboard_press('e',0);
  delay(delay_time);
  keyboard_press('f',0);
  delay(delay_time);
  keyboard_press('g',0);
  delay(delay_time);
  keyboard_press('h',0);
  delay(delay_time);
  keyboard_press('i',0);
  delay(delay_time);
  keyboard_press('j',0);
  delay(delay_time);
  keyboard_press('k',0);
  delay(delay_time);
  keyboard_press('l',0);
  delay(delay_time);
  keyboard_press('m',0);
  delay(delay_time);
  keyboard_press('n',0);
  delay(delay_time);
  keyboard_press('o',0);
  delay(delay_time);
  keyboard_press('p',0);
  delay(delay_time);
  keyboard_press('q',0);
  delay(delay_time);
  keyboard_press('r',0);
  delay(delay_time);
  keyboard_press('s',0);
  delay(delay_time);
  keyboard_press('t',0);
  delay(delay_time);
  keyboard_press('u',0);
  delay(delay_time);
  keyboard_press('v',0);
  delay(delay_time);
  keyboard_press('w',0);
  delay(delay_time);
  keyboard_press('x',0);
  delay(delay_time);
  keyboard_press('y',0);
  delay(delay_time);
  keyboard_press('z',0);
  delay(delay_time);
  keyboard_press(0x2D,0); // -
  delay(delay_time);
  keyboard_press(0x3D,0); // =
  delay(delay_time);
  keyboard_press(0x5B,0); // [
  delay(delay_time);
  keyboard_press(0x5D,0); // ]
  delay(delay_time);
  keyboard_press(0x5C,0); // \
  delay(delay_time);
  keyboard_press(0x3B,0); // ;
  delay(delay_time);
  keyboard_press(0x27,0); // '
  delay(delay_time);
  keyboard_press(0x60,0); // `
  delay(delay_time);
  keyboard_press(0x2C,0); // ,
  delay(delay_time);
  keyboard_press(0x2E,0); // .
  delay(delay_time);
  keyboard_press(0x2F,0); // /
  delay(delay_time);
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Run through the keyboard keys with the shift key on.
  keyboard_press('a',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('b',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('c',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('d',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('e',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('f',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('g',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('h',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('i',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('j',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('k',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('l',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('m',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('n',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('o',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('p',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('q',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('r',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('s',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('t',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('u',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('v',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('w',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('x',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('y',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press('z',KEY_LEFT_SHIFT);
  delay(delay_time);
  keyboard_press(0x2D,KEY_LEFT_SHIFT); // -
  delay(delay_time);
  keyboard_press(0x3D,KEY_LEFT_SHIFT); // =
  delay(delay_time);
  keyboard_press(0x5B,KEY_LEFT_SHIFT); // [
  delay(delay_time);
  keyboard_press(0x5D,KEY_LEFT_SHIFT); // ]
  delay(delay_time);
  keyboard_press(0x5C,KEY_LEFT_SHIFT); // \
  delay(delay_time);
  keyboard_press(0x3B,KEY_LEFT_SHIFT); // ;
  delay(delay_time);
  keyboard_press(0x27,KEY_LEFT_SHIFT); // '
  delay(delay_time);
  keyboard_press(0x60,KEY_LEFT_SHIFT); // `
  delay(delay_time);
  keyboard_press(0x2C,KEY_LEFT_SHIFT); // ,
  delay(delay_time);
  keyboard_press(0x2E,KEY_LEFT_SHIFT); // .
  delay(delay_time);
  keyboard_press(0x2F,KEY_LEFT_SHIFT); // /
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Copy everything that has been written to the clipboard
  keyboard_press('a',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press('c',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("English test");
  lcd.setCursor(0, 1);
  lcd.print("complete");
  delay(5000);
}

void test_arabic(int type_speed){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Arabic test");
  lcd.setCursor(0, 1);
  lcd.print("in progress");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad
  start_process("notepad");
  delay(1000);
  
  // Test out number keys first.
  keyboard_press(0xB0,0); //arabic 0
  delay(delay_time);
  keyboard_press(0xB1,0); //arabic 1
  delay(delay_time);
  keyboard_press(0xB2,0); //arabic 2
  delay(delay_time);
  keyboard_press(0xB3,0); //arabic 3
  delay(delay_time);
  keyboard_press(0xB4,0); //arabic 4
  delay(delay_time);
  keyboard_press(0xB5,0); //arabic 5
  delay(delay_time);
  keyboard_press(0xB6,0); //arabic 6
  delay(delay_time);
  keyboard_press(0xB7,0); //arabic 7
  delay(delay_time);
  keyboard_press(0xB8,0); //arabic 8
  delay(delay_time);
  keyboard_press(0xB9,0); //arabic 9
  delay(delay_time);
  keyboard_press(0xBA,0); //arabic letter feh
  delay(delay_time);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Arabic test");
  lcd.setCursor(0, 1);
  lcd.print("complete");
  delay(5000);
}

void test_loop(int type_speed, long KEY_END_number){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Loop test");
  lcd.setCursor(0, 1);
  lcd.print("in progress:");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad twice alt+tab between them.
  start_process("notepad");
  delay(1000);
  start_process("notepad");
  delay(1000);

  // Start repeating the character "1" over and over again for the specified number of times.
  // After every 100, alt-tab to the other notepad window and hit enter and continue.
  long x=0;
  int z=0;
  do {
    keyboard_press('1',0);
    delay(delay_time);
    x=x+1;
    if (z < 100) {
      z=z+1;
    }
    else {
      lcd.setCursor(13, 1);
      int percentage=(float)x/KEY_END_number*100;
      lcd.print(percentage);
      if (percentage < 10) lcd.setCursor(14, 1);
      else lcd.setCursor(15, 1);
      lcd.print("%");
      keyboard_press(KEY_TAB,KEY_LEFT_ALT);
      delay(500);
      z=0;
      keyboard_press(KEY_RETURN,0);
      delay(delay_time);
    }
  } while ( x < KEY_END_number );
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Loop test");
  lcd.setCursor(0, 1);
  lcd.print("complete");
  delay(5000);
}

/*
####################################################################
System functions, dont modify unless you know what your doing
####################################################################
*/
// Used for the txt to speech module
void txt2speech(String txt2say){
  emicSerial.print('S');
  emicSerial.print(txt2say);
  emicSerial.print('\n');
  while (emicSerial.read() != ':');
}

// Used to clean up test code so we don't have to put all of this code for every character we want to run against.
void keyboard_press(int key, int modifier){
  Keyboard.press(modifier);
  Keyboard.press(key);
  Keyboard.releaseAll();
}

// Used to start a process by using the windows key + r to run, then typing in the name passed as an arguement and hit enter
void start_process(String process){
  keyboard_press('r',KEY_LEFT_GUI);
  delay(500);
  Keyboard.print(process);
  delay(500);
  keyboard_press(KEY_RETURN,0);
  delay(500);
  // Set new window to full screen (<alt> + <space> + x)
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press(0x20); // space bar
  Keyboard.press('x');
  Keyboard.releaseAll();
}

// Used to calculate milisecond delay between keyboard presses based on words per minute input
int word_per_minute(int rate){
  // Average number of characters in english words is 4.5
  // Add the rate to the results to compute the addition of spaces
  int wpm = (rate * 4.5) + rate;
  // Now find characters per minute by deviding words per minute by 60
  int cpm = wpm / 60;
  // Finially devide 1000 by cpm to produce number of miliseconds to delay between key presses
  return 1000 / cpm;
} 

/* 
Move the mouse to the upper left corner to start. 
Use 2550 so that it SHOULD work on any resolution monitor
Positive X moves to the right. Positive Y moves downwards
*/
void mouse_reset(){
  int i=0;
  for (i=0; i<2550; i++) {
    Mouse.move(-2, -2);
    delay(1);
  }
}

/* 
Move the mouse to the specified x and y coordinates on the screen. 
This is not EXACT due to pixel width and other factors. Hence
having to multiply by .76.
*/
void mouse_move(int x, int y){
  int i=0;
  // Move x axis first
  // Had to multiply in order to get to exact location on screen.
  for (i=0; i<(x*.76); i++) {
    Mouse.move(2, 0);
    delay(1);
  }
  i=0;
  // Now move y axis
  // Had to multiply in order to get to exact location on screen.
  for (i=0; i<(y*.77); i++) {
    Mouse.move(0, 2);
    delay(1);
  }
}

void display_countdown(int delay_seconds){
  // set the cursor to column 0, line 0
  // (note: line 1 is the second row, since counting begins with 0):
  int incriment=100/delay_seconds;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Please wait,");
  lcd.setCursor(0, 1);
  lcd.print("initializing");
  int x=0;
  int percentage=0;
  do {
    lcd.setCursor(13, 1);
    percentage=percentage+incriment;
    lcd.print(percentage);
    delay(1000);
    x=x+1;
  } while ( x < delay_seconds );
}

int get_key(unsigned int input)
{
    int k;
    for (k = 0; k < NUM_KEYS; k++)
    {
      if (input < adc_key_val[k])
      {
        return k;
      }
    }   
    if (k >= NUM_KEYS)k = -1;  // No valid key pressed
    return k;
}
