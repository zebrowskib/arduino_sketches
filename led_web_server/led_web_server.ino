#include <Ethernet.h>
#include <SPI.h>
#include <SoftwareSerial.h>
boolean reading = false;

byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0x9C, 0x3B };  // MAC address from Ethernet shield sticker under board
IPAddress ip(192,168,0,25);                           // IP address
IPAddress gateway(192,168,0,1);                       // Gateway address
IPAddress subnet(255,255,255,0);                      // Subnet mask

EthernetServer server = EthernetServer(80); //port 80

// Setup connections for txt 2 speech module to work.
#define rxPin 3  // Serial input (connects to Emic 2 SOUT)
#define txPin 4  // Serial output (connects to Emic 2 SIN)
SoftwareSerial emicSerial = SoftwareSerial(rxPin, txPin); // Set up a new serial port

/*
First option is for how many words per minute you want it to type at.
  NOTE: Guiness book of world records states the fasts was around 180 words per minute, 
        this is our baseline
Second option is how many times the character "1" will be typed onto the two notepad 
docs before we are done.
*/
int type_rate=180;
long type_loop=130000;

void setup(){
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
}

void loop(){

  // listen for incoming clients, and process qequest.
  checkForClient();

}

void checkForClient(){

  EthernetClient client = server.available();

  if (client) {

    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean sentHeader = false;

    while (client.connected()) {
      if (client.available()) {

        if(!sentHeader){
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          // send web page
          client.println("<!DOCTYPE html>");
          client.println("<html>");
          client.println("<head>");
          client.println("<title>Arduino Keyboard Test</title>");
          client.println("</head>");
          client.println("<body>");
          client.println("testing txt<br>");
          client.println("the value of the url is: ");
          client.println("</body>");
          client.println("</html>");
          sentHeader = true;
        }

        char c = client.read();

        if(reading && c == ' ') reading = false;
        if(c == '?') reading = true; //found the ?, begin reading the info

        if(reading){
           switch (c) {
            case '2':
              //add code here to trigger on 2
              triggerPin(2, client);
              break;
            case '3':
            //add code here to trigger on 3
              triggerPin(3, client);
              break;
            case '4':
            //add code here to trigger on 4
              triggerPin(4, client);
              break;
            case '5':
            //add code here to trigger on 5
              triggerPin(5, client);
              break;
            case '6':
            //add code here to trigger on 6
              triggerPin(6, client);
              break;
            case '7':
            //add code here to trigger on 7
              triggerPin(7, client);
              break;
            case '8':
            //add code here to trigger on 8
              triggerPin(8, client);
              break;
            case '9':
            //add code here to trigger on 9
              triggerPin(9, client);
              break;
          }

        }

        if (c == '\n' && currentLineIsBlank)  break;

        if (c == '\n') {
          currentLineIsBlank = true;
        }else if (c != '\r') {
          currentLineIsBlank = false;
        }

      }
    }

    delay(1); // give the web browser time to receive the data
    client.stop(); // close the connection:

  } 

}

void triggerPin(int pin, EthernetClient client){
//blink a pin - Client needed just for HTML output purposes.  
  client.print("Turning on pin ");
  client.println(pin);
  client.print("<br>");

  digitalWrite(pin, HIGH);
  delay(25);
  digitalWrite(pin, LOW);
  delay(25);
}
