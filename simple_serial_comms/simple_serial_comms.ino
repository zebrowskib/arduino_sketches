void setup()
{
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
}

void loop()
{
  while (Serial.available() > 0) {
    Serial.write(Serial.read());
  }
}
