#include <Ethernet.h>
#include <SPI.h>

// This is used for the ethernet shield.
boolean reading = false;

// Setup pins to be used for relay(s).
// If your program doesn't act right, its most likely a power issue
// With the addition of the ethernet shield, the board takes more power to run.
// Adding a 9v battery to the board and disconnecting USB seems to fix the problem.
#define relay1Pin A0     // Control pin for relay 1
//#define relay2Pin A4

// Setup the ethernet shield (NOTE: You MUST change this MAC address for EACH shield)
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0x7C, 0xCB };  // MAC address from Ethernet shield sticker under board
IPAddress ip(192,168,0,24);                           // IP address
IPAddress gateway(192,168,0,1);                       // Gateway address
IPAddress subnet(255,255,255,0);                      // Subnet mask
EthernetServer server = EthernetServer(80);           // Port 80

void setup(){
  pinMode(relay1Pin, OUTPUT);
  //pinMode(relay2Pin, OUTPUT);
  
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
}

void loop(){
  EthernetClient client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean sentHeader = false;

    while (client.connected()) {
      if (client.available()) {
        if(!sentHeader){
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          // send web page
          client.println("<!DOCTYPE html>");
          client.println("<html><head>");
          client.println("<style type=\"text/css\">body {background-color:black;color:white;font-family:\"Courier\";}</style>");
          client.println("<title>Arduino Test</title>");
          client.println("</head><body>");
          client.println("Relay 1 Status:");
          client.println(relay_status(relay1Pin));
          client.println("<button onclick=\"location.href='/?1'\">Change Relay 1</button><br>");
          //client.println("Relay 2 Status:");
          //client.println(relay_status(relay2Pin));
          //client.println("<br><button onclick=\"location.href='/?2'\">Change Relay 2</button><br>");
          //client.println("</body></html>");
          sentHeader = true;
        }

        char c = client.read();

        if(reading && c == ' ') reading = false;
        if(c == '?') reading = true; //found the ?, begin reading the info

        if(reading){
          /*
          This is where we define which test we want to run based 
          on the ID number passed in the url. If you want the test
          to print out text to the user to let them know the test
          is running, you MUST include "client" as an option as the
          last arguement and include that in your function.
          */
          if (c == '1'){
            change_relay_status(relay1Pin);
            delay(100);
            client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
          }
          //else if (c == '2'){
            //change_relay_status(relay2Pin);
            //client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
          //}
        }
        
        if (c == '\n' && currentLineIsBlank)  break;
        if (c == '\n') {
          currentLineIsBlank = true;
        }else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(5); // give the web browser time to receive the data
    client.stop(); // close the connection:
  } 
}
/*
####################################################################
System functions, dont modify unless you know what your doing
####################################################################
*/
String relay_status(int relay_pin) {
  if (digitalRead(relay_pin)){
    return "<font color=\"green\">ON</font>";
  }else{
    return "<font color=\"red\">OFF</font>";
  }
}

void change_relay_status(int relay_pin) {
  if (digitalRead(relay_pin))
    digitalWrite(relay_pin, LOW);        //If the pin is currently ON, turn it off
  else
    digitalWrite(relay_pin, HIGH);       //If the pin is currently OFF, turn it on
}
