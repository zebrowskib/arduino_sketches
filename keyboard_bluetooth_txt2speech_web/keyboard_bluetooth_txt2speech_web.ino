#include <Ethernet.h>
#include <SPI.h>
#include <SoftwareSerial.h>
/*
NOTE: We are writing all arrays for the keyboard presses to FLASH due to the fact
that the leonardo has very limited space in SRAM. If we don't, it will fail in
unpredictable ways.
Also note that to power all of the devices attached for this sketch, you will need
a wall wart. Otherwise most everything will power on via standard USB, but the 
bluetooth module will NOT. I am using a 6v 450ma adapter and it seems to be working
fine now.
*/
#include <avr/pgmspace.h>

// This is used for the ethernet shield.
boolean reading = false;

// Setup connections for txt 2 speech module to work.
#define emicrxPin 8                                            // Serial input (connects to Emic 2 SOUT)
#define emictxPin 7                                            // Serial output (connects to Emic 2 SIN)
SoftwareSerial emicSerial = SoftwareSerial(emicrxPin, emictxPin);  // Set up a new serial port

// Setup connections for bluetooth module to work
#define bluetoothtxPin 9                                      // Serial input (connects to bluetooth tx. Not actually used.)
#define bluetoothrxPin 6                                      // Serial output (connects to bluetooth rx)
#define bluetoothpwrPin A0                                    // 5v power pin so you can turn bluetooth on and off at will.
SoftwareSerial bluetoothSerial = SoftwareSerial(bluetoothtxPin, bluetoothrxPin);

// Setup the ethernet shield (NOTE: You MUST change this MAC address for EACH shield)
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0E, 0x9C, 0x3B };  // MAC address from Ethernet shield sticker under board
IPAddress ip(192,168,0,25);                           // IP address
IPAddress gateway(192,168,0,1);                       // Gateway address
IPAddress subnet(255,255,255,0);                      // Subnet mask
EthernetServer server = EthernetServer(80);           //port 80

int type_rate=180;      //How many words per minute you want it to type at
                        //NOTE: Guiness book of world records states the fasts was around 180 words 
                        //per minute, this is our baseline.
long type_loop=15000;
//long type_loop=130000;  //How many times the character "1" will be typed onto the two notepad docs 
                        //before were done.
                        
/*
Create an array for each type of keyboard key you want to press.
It is broken down for easy of reading on the screen. These are used
later for the individual tests. But they are being written to FLASH
to give room to SRAM. So calling them will be slightly differen't
then normal.
*/
// Normal keyboard keys
char en_number_array[10] PROGMEM = {'1','2','3','4','5','6','7','8','9','0'};
// Keyboard based on raw USB HID keycodes. NOT ASCII!
char en_number_bt_array[10] PROGMEM = {0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27};
// Normal keyboard keys
char en_letter_array[26] PROGMEM = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
// Keyboard based on raw USB HID keycodes. NOT ASCII!
char en_letter_bt_array[26] PROGMEM = {0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D};
// Use ascii characters for special characters: - = [ ] \ ; ' ` , . /
char en_special_array[11] PROGMEM = {0x2D,0x3D,0x5B,0x5D,0x5C,0x3B,0x27,0x60,0x2C,0x2E,0x2F};
// Special characters based on raw USB HID keycodes. NOT ASCII!
char en_special_bt_array[11] PROGMEM = {0x2D,0x2E,0x2F,0x30,0x31,0x33,0x34,0x35,0x36,0x37,0x38};

// Arabic keys copied from win7 character map program.
// www.ascii.ca/cp864.htm
// Still working on this so far.
//char ar_letter_array[33] PROGMEM = {'ہ','ء','ا','ب','ة','ت','ث','ج','ح','خ','د','ذ','ر','ز','س','ش','ص','ض','ط','ظ','ع','غ','ف','ق','ك','ل','م','ن','ه','و','ى','ي','ے'};
char ar_letter_array[28] PROGMEM = {0xA2,0xA5,0xA8,0xA9,0xAA,0xAB,0xAD,0xAE,0xAF,0xBA,0xBC,0xBD,0xBE,0xC1,0xC2,0xC3,0xC4,0xC5,0xC6,0xC7,0xC8,0xC9,0xCA,0xCB,0xCD,0xCE,0xCF,0xD0};
char ar_number_array[10] PROGMEM = {0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xb8,0xB9};

/*
####################################################################
System functions, dont modify unless you know what your doing
####################################################################
*/
// Used for the txt to speech module
void txt2speech(String txt2say){
  emicSerial.print('S' + txt2say + '\n');
  while (emicSerial.read() != ':');
}

// Used to clean up test code so we don't have to put all of this code for every character we want to run against.
void keyboard_press(char key, char modifier){
  Keyboard.press(modifier);
  Keyboard.press(key);
  Keyboard.releaseAll();
}

// Used to clean up test code so we don't have to put all of this code for every character we want to run against.
void bluetooth_keyboard_press(uint8_t keycode, uint8_t modifier = 0) {
  bluetoothSerial.write(0xFD);
  bluetoothSerial.write(modifier);  // modifier!
  bluetoothSerial.write((byte)0x00); // 0x00  
  bluetoothSerial.write(keycode);   // key code
  bluetoothSerial.write((byte)0x00); // 0x00 
  bluetoothSerial.write((byte)0x00); // 0x00 
  bluetoothSerial.write((byte)0x00); // 0x00 
  bluetoothSerial.write((byte)0x00); // 0x00 
  bluetoothSerial.write((byte)0x00); // 0x00 
}

// Used to start a process by using the windows key + r to run, then typing in the name passed as an arguement and hit enter
void start_process(String process){
  keyboard_press('r',KEY_LEFT_GUI);
  delay(1000);
  Keyboard.print(process);
  delay(500);
  keyboard_press(KEY_RETURN,0);
  delay(2000);
  // Set new window to full screen (<alt> + <space> + x)
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press(0x20); // space bar
  Keyboard.press('x');
  Keyboard.releaseAll();
}

// Used to close the current window selected. Be carefull when using this, it could close out the wrong window.
void close_current_process(){
  keyboard_press('f',KEY_LEFT_ALT);
  delay(500);
  keyboard_press('x',0);
  delay(500);
  keyboard_press('n',0);
}

// Used to calculate milisecond delay between keyboard presses based on words per minute input
int word_per_minute(int rate){
  // Average number of characters in english words is 4.5
  // Add the rate to the results to compute the addition of spaces
  int wpm = (rate * 4.5) + rate;
  // Now find characters per minute by deviding words per minute by 60
  int cpm = wpm / 60;
  // Finially devide 1000 by cpm to produce number of miliseconds to delay between key presses
  return 1000 / cpm;
} 

/* 
Move the mouse to the upper left corner to start. 
Use 2550 so that it SHOULD work on any resolution monitor
Positive X moves to the right. Positive Y moves downwards
*/
void mouse_reset(){
  for (int i=0; i<2550; i++) {
    Mouse.move(-2, -2);
    delay(1);
  }
}

/* 
Move the mouse to the specified x and y coordinates on the screen. 
Set the following registry keys to disable mouse acceleration.
HKCU/Control Panel/Mouse/
MouseThreshold1 = 0
MouseThreshold2 = 0
MouseSpeed = 0
*/
void mouse_move(int x, int y){
  // Move x axis first
  // Had to multiply in order to get to exact location on screen.
  for (int i=0; i<(x*.5); i++) {
    Mouse.move(2, 0);
    delay(1);
  }
  // Now move y axis
  // Had to multiply in order to get to exact location on screen.
  for (int i=0; i<(y*.5); i++) {
    Mouse.move(0, 2);
    delay(1);
  }
}

// Used to check a pins power status. Primarly to tell if bluetooth is off or on
String power_status(char power_pin) {
  if (digitalRead(power_pin)){
    return "<font color=\"green\">ON</font>";
  }else{
    return "<font color=\"red\">OFF</font>";
  }
}

// Used to change a pin status from on to off or vice versa.
void change_bluetooth_status(char power_pin, EthernetClient client) {
  if (digitalRead(power_pin)){
    digitalWrite(power_pin, LOW);           //If the pin is currently ON, turn it off
    client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">"); 
  }
  else {
    digitalWrite(power_pin, HIGH);          //If the pin is currently OFF, turn it on
    delay(10000);
    bluetoothSerial.begin(9600);
    client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">"); 
  }
}

/* 
####################################################################
Start Defining what tests you want to perform (no particular order)
####################################################################
*/
/*
This will test almost all keyboard keys over bluetooth HID.
Note that after each round, enter key is pressed to keep output clean.
*/
void test_bluetooth_keyboard(int type_speed, EthernetClient client){
  client.println("English function test in progress.<br>");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);
  // Test out number keys first using the number array from above.
  for (byte i=0; i <= 9; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_number_bt_array[i]),0);
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  bluetoothSerial.write(0x0A); // hit enter key
  delay(delay_time);
  
  // Now number keys with shift on
  for (byte i=0; i <= 9; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_number_bt_array[i]),(1<<1));
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  bluetoothSerial.write(0x0A); // hit enter key
  delay(delay_time);
  
  // Test out arrow keys just to verify
  bluetoothSerial.write(0x0E); // Up arrow
  delay(delay_time);
  bluetoothSerial.write(0x0B); // Left arrow
  delay(delay_time);
  bluetoothSerial.write(0x0C); // Down arrow
  delay(delay_time);
  bluetoothSerial.write(0x07); // Right arrow
  delay(delay_time);
  bluetoothSerial.write(0x05); // End key
  delay(delay_time);
  bluetoothSerial.write(0x0A);
  delay(delay_time);
  
  // Start typing the keys on the keyboard
  for (byte i=0; i <= 25; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_letter_bt_array[i]),0);
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  // Now special characters
  for (byte i=0; i <= 10; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_special_bt_array[i]),0);
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  bluetoothSerial.write(0x0A); // hit enter key
  delay(delay_time);
  
  // Same thing as above, but with caps lock on
  bluetoothSerial.write(0x1C); // hit caps lock
  // Start typing the keys on the keyboard
  for (byte i=0; i <= 25; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_letter_bt_array[i]),0);
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_special_bt_array[i]),0);
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  bluetoothSerial.write(0x0A); // hit enter key
  delay(delay_time);
  bluetoothSerial.write(0x1C); // hit caps lock
  
  // One last time, this time with the left shift key pressed for each key.
  for (byte i=0; i <= 25; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_letter_bt_array[i]),(1<<1));
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    bluetooth_keyboard_press(pgm_read_word_near(&en_special_bt_array[i]),(1<<1));
    bluetooth_keyboard_press(0,0);
    delay(delay_time);
  }
  bluetoothSerial.write(0x0A); // hit enter key
  delay(delay_time);
  
  // Copy everything to the clipboard
  bluetooth_keyboard_press(0x04,(1<<0)); // <ctrl>+a
  bluetooth_keyboard_press(0,0);
  delay(delay_time);
  bluetooth_keyboard_press(0x06,(1<<0)); // <ctrl>+c
  bluetooth_keyboard_press(0,0);
  delay(delay_time);
  bluetoothSerial.write(0x05); // End key
  delay(delay_time);
  
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
}

/*
This will test almost all keyboard keys over USB.
Note that after each round, enter key is pressed to keep output clean.
*/
void test_english(int type_speed, EthernetClient client){
  client.println("English function test in progress.<br>");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad so we can type in it.
  start_process("notepad");
  delay(1000);
  
  // Test out number keys first using the number array from above.
  for (byte i=0; i <= 9; i++){
    keyboard_press(pgm_read_word_near(&en_number_array[i]),0);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Now number keys with shift on
  for (byte i=0; i <= 9; i++){
    keyboard_press(pgm_read_word_near(&en_number_array[i]),KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);

  // Test out arrow keys just to verify
  keyboard_press(KEY_UP_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_LEFT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_DOWN_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_RIGHT_ARROW,0);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Start typing the keys on the keyboard
  for (byte i=0; i <= 25; i++){
    keyboard_press(pgm_read_word_near(&en_letter_array[i]),0);
    delay(delay_time);
  }
  //Start typing special keys
  for (byte i=0; i <= 10; i++){
    keyboard_press(pgm_read_word_near(&en_special_array[i]),0);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Same thing as above, but with caps lock on
  keyboard_press(KEY_CAPS_LOCK,0);
  for (byte i=0; i <= 25; i++){
    keyboard_press(pgm_read_word_near(&en_letter_array[i]),0);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    keyboard_press(pgm_read_word_near(&en_special_array[i]),0);
    delay(delay_time);
  }
  keyboard_press(KEY_CAPS_LOCK,0);
  delay(delay_time);
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // One last time, this time with the left shift key pressed for each key.
  for (byte i=0; i <= 25; i++){
    keyboard_press(pgm_read_word_near(&en_letter_array[i]),KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  for (byte i=0; i <= 10; i++){
    keyboard_press(pgm_read_word_near(&en_special_array[i]),KEY_LEFT_SHIFT);
    delay(delay_time);
  }
  keyboard_press(KEY_RETURN,0);
  delay(delay_time);
  
  // Copy everything that has been written to the clipboard
  keyboard_press('a',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press('c',KEY_LEFT_CTRL);
  delay(delay_time);
  keyboard_press(KEY_END,0);
  delay(delay_time);
  
  close_current_process();
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
}

/*
This will test almost all keyboard keys for arabic
Currently it doesn't appear that the arduino leonardo support the extended
ascii table to do these characters.
*/
void test_arabic(int type_speed, EthernetClient client){
  client.println("Arabic function test in progress.<br>");
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad
  start_process("notepad");
  delay(1000);
  
  // Test out number keys first.
  for (byte i=0; i <= 10; i++){
    keyboard_press(pgm_read_word_near(&ar_number_array[i]),0);
    delay(delay_time);
  }
  
  close_current_process();
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
}

//This will repeat the character "1" over and over again for the specified number of times.
//After every 100, alt-tab to the other notepad window and hit enter and continue.
void test_loop(int type_speed, long KEY_END_number, EthernetClient client){
  client.println("Loop function test in progress.<br>");
  client.println("|---10---20---30---40---50---60---70---80---90---|<br>");
  
  // Use the word_per_minute function to find milisecond delay based on WPM input
  int delay_time = word_per_minute(type_speed);

  // Start notepad twice alt+tab between them.
  start_process("notepad");
  delay(1000);
  start_process("notepad");
  delay(1000);
  
  // Set some place holders for use later
  byte last_print_percent = -1;
  byte z=0;
  
  do {
    keyboard_press('1',0);
    delay(delay_time);
    // We are going to count down from our starting number we specified above.
    KEY_END_number--;
    if (z < 99) {
      z++;
    }
    else {

      // This will figure out our current progress towards completion and print a -
      // to show progress after every 2 percent.
      byte percentage=(float)KEY_END_number/type_loop*100;
      txt2speech((String)percentage);
      if (percentage % 2 == 0) { //Determines if the percentage is evenly divisable by 2.
        // This will verify we don't double print a - for progress if the percentage is the same.
        if (percentage != last_print_percent) {
          last_print_percent = percentage;
          client.print("-");
        }
      }

      keyboard_press(KEY_TAB,KEY_LEFT_ALT);
      delay(500);
      z=0;
      keyboard_press(KEY_RETURN,0);
      delay(delay_time);
    }
  } while ( KEY_END_number >= 0 );
  
  close_current_process();
  close_current_process();
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">");
}

void test_mouse(int x, int y, EthernetClient client) {
  // Move the mouse to the upper left corner, then the specified position. 
  // You must reset ever time in order to get to your destination accurately.
  client.println("Mouse function test in progress.<br>");
  mouse_reset();
  mouse_move(x,y); 
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">"); 
}

void test_speech(String txt2say, EthernetClient client) {
  client.println("Speech function test in progress.<br>You should hear the following text played: ");
  client.println(txt2say);
  txt2speech(txt2say);
  client.print("<meta http-equiv=\"refresh\" content=\"0;url=/\">"); 
}

void setup() {
  // Start up the ethernet shield
  Ethernet.begin(mac, ip, gateway, subnet);
  server.begin();
  
  // In case we want to debug, turn on serial out
  //Serial.begin(9600);
  
  // Allow the power pin for the bluetooth to output 5v. 
  // NOTE: This doesn't actually set it to on, that is done via the web interface!
  pinMode(bluetoothpwrPin, OUTPUT);
  
  // Start up the txt 2 speech module and configure some options
  pinMode(emicrxPin, INPUT);
  pinMode(emictxPin, OUTPUT);
  emicSerial.begin(9600);
  emicSerial.print('\n');              // Send a CR in case the system is already up
  while (emicSerial.read() != ':');    // When the Emic 2 has initialized and is ready, it will send a single ':' 
                                       // character, so wait here until we receive it
  delay(10);                           // Short delay
  emicSerial.flush();                  // Flush the receiver buffer
  emicSerial.print("V18\n");           // Set the volume (range from -48 to 18)
  while (emicSerial.read() != ':');
  emicSerial.print("N1\n");            // Set the voice (range from 0 to 8)
  while (emicSerial.read() != ':');
  emicSerial.print("L0\n");            // Set the language (0=english, 1=castilian spanish, 2=latin spanish)
  while (emicSerial.read() != ':');
  emicSerial.print("W230\n");          // Set the speech rate (range from 75 to 600)
  while (emicSerial.read() != ':');
}

void loop() {
  EthernetClient client = server.available();
  if (client) {
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    boolean sentHeader = false;

    while (client.connected()) {
      if (client.available()) {
        if(!sentHeader){
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          // send web page
          client.println("<!DOCTYPE html>");
          client.println("<html><head>");
          client.println("<style type=\"text/css\">body {background-color:black;color:white;font-family:\"Courier\";}</style>");
          client.println("<title>Arduino Test</title>");
          client.println("</head><body>");
          //client.println("Please note that for mouse movements it is REQUIRED that the following criteria be met:");
          //client.println("<ul><li>Modify the following reg keys to disable mouse acceleration per user profile.</li>");
          //client.println("<ul><li>HKCU/Control Panel/Mouse/");
          //client.println("<ul><li>MouseThreshold1 = 0</li><li>MouseThreshold2 = 0</li><li>MouseSpeed = 0</li>");
          //client.println("</ul></ul></ul>");
          client.println("Available tests:<br>");
          // Start listing the tests you want to run, single letter/number ONLY. This letter/number
          // MUST match in the below IF statement if you want the test to actually run.
          client.println("<button onclick=\"location.href='?0'\">Loop test</button><br>");
          client.println("<button onclick=\"location.href='?1'\">English test</button><br>");
          //client.println("<button onclick=\"location.href='?2'\">Arabic test</button><br>");
          client.println("<button onclick=\"location.href='?3'\">Mouse test</button><br>");
          client.println("<button onclick=\"location.href='?4'\">Speech test</button><br>");
          client.println("<button onclick=\"location.href='?a'\">Bluetooth keyboard test</button><br>");
          client.println("<button onclick=\"location.href='?5'\">Change Status</button> Bluetooth:");
          client.println(power_status(bluetoothpwrPin));
          
          client.println("<hr>The current test is: ");
          sentHeader = true;
        }

        char c = client.read();

        if(reading && c == ' ') reading = false;
        if(c == '?') reading = true; //found the ?, begin reading the info

        if(reading){
          /*
          This is where we define which test we want to run based 
          on the ID number passed in the url. If you want the test
          to print out text to the user to let them know the test
          is running, you MUST include "client" as an option as the
          last arguement and include that in your function.
          */
          if (c == '0') {
            Keyboard.begin();
            test_loop(type_rate,type_loop,client);
            Keyboard.end(); }
          else if (c == '1') {
            Keyboard.begin();
            test_english(type_rate,client);
            Keyboard.end(); }
          else if (c == '2'){
            Keyboard.begin();
            test_arabic(type_rate,client);
            Keyboard.end(); }
          else if (c == '3') {
            Mouse.begin();
            test_mouse(500,600,client);
            Mouse.end(); }
          else if (c == '4')
            test_speech("Testing the text to speech module on the arduino.",client);
          else if (c == '5')
            change_bluetooth_status(bluetoothpwrPin,client);
          else if (c == 'a')
            test_bluetooth_keyboard(type_rate,client);
        }
        
        if (c == '\n' && currentLineIsBlank)  break;
        if (c == '\n') {
          currentLineIsBlank = true;
        }else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(50); // give the web browser time to receive the data
    client.stop(); // close the connection:
  } 
}
