#include <Servo.h> 

/* To use this, connect the control wire from the servo to either digital ping 6 or 7
Then use a degree as what position you want the servo to turn to. Positive goes to 5V.
Servo myservo1;
Servo myservo2;
 
void setup() 
{ 
  myservo1.attach(6);  // attaches the servo on pin 9 to the servo object
  myservo2.attach(7);  // attaches the servo on pin 9 to the servo object 
} 
 
void loop() 
{ 
  myservo1.write(45);
  delay(1000);
  myservo1.write(90);
  myservo2.write(179);
  delay(3000);
  myservo1.write(0);
  myservo2.write(0);
  delay(3000);
} 
