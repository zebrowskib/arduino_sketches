/* YourDuinoStarter Example: RGB_LED_Fade
 - Red-Green-Blue LED fades between colors automatically
 - SEE the comments after "//" on each line below
 - CONNECTIONS: NOTE: Hold RGB LED with pins down and longer pin
                second from the Left. 4 Pins in order left-right:
   - Left:  RED
   - Second: +5V "Common Anode"
   - Third: BLUE
   - Right: Green
 - V1.00 09/17/12
   Based on code by Matthew L Beckler
   Questions: terry@yourduino.com */

/*-----( Import needed libraries )-----*/
//none
/*-----( Declare Constants and Pin Numbers )-----*/
#define redPin 11  
#define bluePin 10
#define greenPin 9
#define powerPin 6
#define tonePin 8

#define brighter  1  //Used by SetLED_brightness function
#define dimmer    0
/*-----( Declare objects )-----*/
/*-----( Declare Variables )-----*/
int  redValue ;  // Hold the current brightness value
int  greenValue;
int  blueValue;

int  updateDelay; // Milliseconds delay on changes


void setup()   /****** SETUP: RUNS ONCE ******/
{
  Serial.begin(9600);
  pinMode(powerPin, OUTPUT);
  digitalWrite(powerPin, HIGH);
  /* NOTE: Because the LED is "common Anode", the LEDs get brighter
   as the Arduino pin goes towards 0 volts. (It is "Inverted").
   So value=255 of OFF and value=0 in brightest.  */
   
  redValue = 255;  // Start with all off
  greenValue = 255;
  blueValue = 255;
  updateDelay = 6; // Ms delay. Higher will be slower. Try 2 also.
  SetLED_brightness();  // This is defined at the end of this text
}//--(end setup )---


void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
 
//  ColorChange(&redValue, brighter ); // Red only
//  ColorChange(&redValue, dimmer ); 
//  delay(updateDelay * 100);  
// 
//  ColorChange(&greenValue, brighter ); // Green only
//  ColorChange(&greenValue, dimmer );   
//  delay(updateDelay * 100);    
  
  ColorChange(&blueValue, brighter ); // Blue only
  ColorChange(&blueValue, dimmer );   

//  ColorChange(&redValue, brighter ); // Red plus Green = Yellow
//  ColorChange(&greenValue, brighter );  
//  delay(updateDelay * 100);  
//  ColorChange(&blueValue, brighter ); // plus Blue 
//  delay(updateDelay * 100);  
//  ColorChange(&redValue, dimmer );   
//  ColorChange(&greenValue, dimmer ); 
//  ColorChange(&blueValue, dimmer );   
//  delay(updateDelay * 200);


}//--(end main loop )---

/*-----( Declare User-written Functions )-----*/

/*------( This function updates the LED outputs)----*/
void SetLED_brightness()
{
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
} // End SetLED_brightness

/*--(Update one of the Colors )--------------------------*/ 

void ColorChange(int* LEDvalue, int howBright)
/*--( Brighten an LED to full, or dim to dark )---*/
{
  for (int i = 0; i < 200; i++)
  {
    if (howBright == dimmer)    (*LEDvalue)++; 
    if (howBright == brighter)  (*LEDvalue)--;
    SetLED_brightness();
    delay(updateDelay);
  }
}

void fullAudoRange(){
  for (int audioTone = 30; audioTone <= 5000; audioTone++) {
    // turn off tone function for pin 8:
    noTone(tonePin);
    // play a note on pin 8 for 500 ms:
    tone(tonePin, audioTone, 500);
    Serial.println(audioTone);
    delay(200);
  }
}

void tardisSound(){
  // turn off tone function for pin 8:
  noTone(tonePin);
  // play a note on pin 8 for 500 ms:
  tone(tonePin, 400, 500);
  delay(500);
  tone(tonePin, 1200, 500);
  delay(500);
  tone(tonePin, 400, 500);
  delay(500);
}
