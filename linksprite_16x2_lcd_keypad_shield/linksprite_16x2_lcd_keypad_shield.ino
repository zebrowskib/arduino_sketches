/*
Reference for continues rotation servos
1700  =  Counter-Clockwise
1300  =  Clockwise
1500  =  Stationary
Anything in between = speed

Turning degrees seems to be close to 600 delay = 90 degree turn.

Here are the connections necessary:
servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10
golf arm---------------------D12

ping sensor------------------Arduino
gnd pin----------------------5
power pin--------------------6
control pin------------------7

*/
// Include libraries for Servo
#include <Servo.h>

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
#define arm_servo_pin 12
Servo servoRight;
Servo servoLeft;
Servo servoArm;

#define pingGndPin 5
#define ping5vPin 6
#define pingPin 7
unsigned int duration, inches;

#include <LiquidCrystal.h>
#include <LCDKeypad.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
char msgs[5][16] = {"Right Key OK ",
                    "Up Key OK    ",               
                    "Down Key OK  ",
                    "Left Key OK  ",
                    "Select Key OK" };
int adc_key_val[5] ={50, 200, 400, 600, 800 };
int NUM_KEYS = 5;
int adc_key_in;
int key=-1;
int oldkey=-1;
 
void setup()
{
  Serial.begin(9600);
  
  // Setup the servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
  servoArm.attach(arm_servo_pin); 
  
  // Set the pins to be used for the ping sensor to output for digitalwrite
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);
  // Set the arm to the start position to begin
  servoArm.write(10);
  
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("     helle! ");
  lcd.print("      welcome!");
  lcd.setCursor(0,1);
  lcd.print("   LinkSprite");
  lcd.print("    LCD Shield");
  delay(1000);
 
  lcd.setCursor(0,0);
  for (char k=0;k<26;k++)
  {
    lcd.scrollDisplayLeft();
    delay(400);
  }
  lcd.clear();
  lcd.setCursor(0,0); 
  lcd.print("ADC key testing"); 
}
void loop()
{
   adc_key_in = analogRead(0);    // read the value from the sensor 
   key = get_key(adc_key_in);  // convert into key press
   if (key != oldkey)   // if keypress is detected
   {
     delay(50);  // wait for debounce time
     adc_key_in = analogRead(0);    // read the value from the sensor 
     key = get_key(adc_key_in);    // convert into key press
     if (key != oldkey)    
     {   
       lcd.setCursor(0, 1);
       oldkey = key;
       if (key >=0)
       {
           lcd.print(msgs[key]);              
       }
     }
   }
   delay(100);
}
// Convert ADC value to key number
int get_key(unsigned int input)
{
    int k;
    for (k = 0; k < NUM_KEYS; k++)
    {
      if (input < adc_key_val[k])
      {
        return k;
      }
    }   
    if (k >= NUM_KEYS)k = -1;  // No valid key pressed
    return k;
}

void go_forward(int distance){
  servoRight.writeMicroseconds(1300);
  servoLeft.writeMicroseconds(1700);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

void go_backward(int distance){
  servoRight.writeMicroseconds(1700);
  servoLeft.writeMicroseconds(1300);
  delay(distance);
  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);
}

int ping_distance(){
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              //   with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  return inches;                     // Return the results
}

void swing_arm(){
  servoArm.write(10);
  delay(1000);
  servoArm.write(170);
  delay(5000);
  servoArm.write(10);
}
