/* !!! Comment out to stop serial prints.  Do this before flashing HID firmware !!! */
#define DEBUG

#define TOTAL_BUTTONS 6          // Total buttons on the steering wheel
#define NONE -1                  // When no button is pressed
#define MUTE 0                   // Index corresponding to the MUTE button resistance
#define VOLUME_UP 1              // Index corresponding to the VOLUME_UP button resistance
#define VOLUME_DOWN 2            // Index corresponding to the VOLUME_DOWN button resistance
#define MODE 3                   // Index corresponding to the MODE button resistance
#define NEXT 4                   // Index corresponding to the NEXT button resistance
#define PREVIOUS 5               // Index corresponding to the PREVIOUS button resistance

#define TOLERANCE_PERCENT 10.f    // Match tolerance
#define RESISTANCE_PIN 0          // Analogue Input on Arduino
#define R_KNOWN 5600.f            // The known resistor
float VOLTS_IN = 5.f;             // Vcc (+5 Volts)

/* Button resistance values and indexes {MUTE, VOLUME_UP, VOLUME_DOWN, MODE, NEXT, PREVIOUS} */
float BUTTONS[TOTAL_BUTTONS] = {390.f, 100.f, 270.f, 820.f, 470.f, 1800.f};

int currentButton = NONE;        // Currently selected button

int rawVolts = 0;                // The raw analogue value
float voltsOut = 0.f;            // Voltage at point between resistors
float resistance = 0.f;          // Unknown resistance.

void setup() {
  Serial.begin(9600);
}

void loop() {
  calculateResistance();
  printResistance();
  detectCurrentButton();
  printCurrentButton();
  delay(1000);
}

// Calculates the resistance.
void calculateResistance() {
  rawVolts = analogRead(RESISTANCE_PIN);            // Read in raw value (0-1023)
  voltsOut = (VOLTS_IN / 1024.f) * float(rawVolts);    // Convert to voltage
  resistance = R_KNOWN*((VOLTS_IN/voltsOut) - 1);    // Calculate the resistance
}

// Prints the resistance (if DEBUG is enabled)
void printResistance() {
#ifdef DEBUG
  Serial.print("Voltage: ");
  Serial.print(voltsOut);            
  Serial.print(", Resistance: ");
  Serial.println(resistance);
#endif
}

// Detects which button is currently pressed.
void detectCurrentButton() {
  for (int i = 0; i < TOTAL_BUTTONS; i++) {
    if (buttonPressed(BUTTONS[i])) {
       currentButton = i;
       break;
    } else {
       currentButton = NONE; 
    }
  }
}

// Prints the current button, only if DEBUG is enabled.
void printCurrentButton() {
#ifdef DEBUG
  Serial.print("Current Button: ");
  switch(currentButton) {
   case MUTE:
    Serial.println("MUTE");
    break;
   case VOLUME_UP:
    Serial.println("VOLUME_UP");
    break;
   case VOLUME_DOWN:
    Serial.println("VOLUME_DOWN");
    break;
   case MODE:
    Serial.println("MODE");
    break;
   case NEXT:
    Serial.println("NEXT");
    break;
   case PREVIOUS:
    Serial.println("PREVIOUS");
    break; 
   default:
    Serial.println("NONE");
  }
#endif
}

// Determines if the current resistance is within tolerance of a button resistance.
boolean buttonPressed(float buttonResistance) {
  float decimalPercent = TOLERANCE_PERCENT / 200.f;
  float highRange = buttonResistance * (1.f + decimalPercent);
  float lowRange = buttonResistance * (1.f - decimalPercent);
  return lowRange <= resistance && resistance <= highRange;
}




