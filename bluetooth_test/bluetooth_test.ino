#include <SoftwareSerial.h>

/* Bluetooth Mate Echo
 by: Jim Lindblom - jim at sparkfun.com
 date: 3/15/11
 license: CC-SA v3.0 - Use this code however you'd like, for any
 purpose. If you happen to find it useful, or make it better, let us know!
 
 This code allows you to send any of the RN-42 commands to the
 Bluetooth Mate via the Arduino Serial monitor. Characters sent
 over USB-Serial to the Arduino are relayed to the Mate, and
 vice-versa.
 
 Here are the connections necessary:
 Bluetooth Mate-----------------Arduino
 CTS-I    (not connected)
 VCC------------------------5V or 3.3V (supplied from pin A0)
 GND--------------------------GND
 TX-O-------------------------D2
 RX-I-------------------------D3
 RTS-O    (not connected)
 
 How to use:
 You can use the serial monitor to send any commands listed in
 the RN-42 Advanced User Manual
 (http://www.sparkfun.com/datasheets/Wireless/Bluetooth/rn-bluetooth-um.pdf)
 to the Bluetooth Mate.
 
 Open up the serial monitor to 9600bps, and make sure the 
 pull-down menu next to the baud rate selection is initially set
 to "No line ending". Now enter the configuration command $$$ in 
 the serial monitor and click Send. The Bluetooth mate should
 respond with "CMD".
 
 The RN-42 module expects a newline character after every command.
 So, once you're in command mode, change the "No line ending"
 drop down selection to "Newline". To test, send a simple command.
 For instance, try looking for other bluetooth devices by sending
 the I command. Type I and click Send. The Bluetooth Mate should
 respond with "Inquiry, COD", follwed by any bluetooth devices
 it may have found.
 
 To exit command mode, either connect to another device, or send
 ---.
 
 The newline and no line ending selections are very important! If
 you don't get any response, make sure you've set that menu correctly.
 */

// We'll use the newsoftserial library to communicate with the Mate
//#include <NewSoftSerial.h>  

#define bluetoothpower 4 // 5v Power pin for bluetooth module

#define bluetoothRx 2  // RX-I pin of bluetooth mate, Arduino D3
#define bluetoothTx 3  // TX-O pin of bluetooth mate, Arduino D2

SoftwareSerial bluetoothSerial(bluetoothTx, bluetoothRx);

void setup() {
  pinMode(bluetoothpower, OUTPUT);    // Use analog pin 0 to power on/off the bluetooth on 
  pinMode(A1, OUTPUT);    // Use analog pin 1 to reset the bluetooth on PIO6.
  Serial.begin(9600);     // Begin the serial monitor at 9600bps
  
  digitalWrite(bluetoothpower, HIGH);    // Switch on
  delay(500);
  Serial.println("Starting...");  
  bluetoothSerial.begin(9600);        // The Bluetooth Mate defaults to 115200bps
  //delay(5000);                     // IMPORTANT DELAY! (Minimum ~276ms)
  //bluetoothSerial.print("$$$");         // Enter command mode
  //delay(5000);                      // IMPORTANT DELAY! (Minimum ~10ms)
  //bluetoothSerial.println("SN,MyDevice");
  //delay(500);
  //bluetoothSerial.println("U,9600,N");  // Temporarily Change the baudrate to 9600, no parity
  //bluetoothSerial.begin(9600);          // Start bluetooth serial at 9600 
}

void loop() {
  if(bluetoothSerial.available()) { 
    // If the bluetooth sent any characters
    // Send any characters the bluetooth prints to the serial monitor
    Serial.println((char)bluetoothSerial.read()); 
  }
  if(Serial.available()) { 
    // If stuff was typed in the serial monitor
    // Send any characters the Serial monitor prints to the bluetooth
    bluetoothSerial.print((char)Serial.read());
  }
  // and loop forever and ever!
  //Serial.println("you typed: " + (char)Serial.read());
  //bluetoothSerial.println("you typed: " + (char)bluetoothSerial.read()); 
  //bluetoothSerial.print((char)Serial.read());
  //Serial.println((char)bluetoothSerial.read());
}


// Switch off, on and reset the bluetooth to factory defaults. Start serial.
void btReboot() {
  byte dt = 20;
  digitalWrite(bluetoothpower, LOW);    // Switch off
  delay(dt);
  digitalWrite(A1, HIGH);    // Set PIO6 high to reset
  delay(dt);
  digitalWrite(bluetoothpower, HIGH);    // Switch on
  delay(dt);

  digitalWrite(A1, LOW);     // Toggle PIO6 3 times to reset
  digitalWrite(A1, HIGH);
  delay(dt);
  digitalWrite(A1, LOW);
  digitalWrite(A1, HIGH);
  delay(dt);
  digitalWrite(A1, LOW);
  digitalWrite(A1, HIGH);

  Serial.println("Starting...");  
  bluetoothSerial.begin(115200);        // The Bluetooth Mate defaults to 115200bps
  delay(320);                     // IMPORTANT DELAY! (Minimum ~276ms)
  bluetoothSerial.print("$$$");         // Enter command mode
  delay(100);                      // IMPORTANT DELAY! (Minimum ~10ms)
  //bluetoothSerial.println("SN,MyDevice");
  //delay(500);
  bluetoothSerial.println("U,9600,N");  // Temporarily Change the baudrate to 9600, no parity
  bluetoothSerial.begin(9600);          // Start bluetooth serial at 9600 
  Serial.print((char)bluetoothSerial.read());
}
