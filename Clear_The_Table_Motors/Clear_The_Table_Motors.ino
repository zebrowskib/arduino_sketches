/*
Origional Idea: 
http://learn.parallax.com/contest/clear-table

Reference for continues rotation servos
180  =  Counter-Clockwise
0  =  Clockwise
90  =  Stationary
Anything in between = speed

Here are the connections necessary:
Motor------------------------Arduino
------------------------------------
Channel A Direction----------12     (set to HIGH for forward, LOW for backwards)
Channel A Brake--------------9      (set to HIGH to engage, LOW to release)
Channel A Speed--------------3      (Set to 1-160)
Channel A Current Sensing----A0
Channel B Direction----------13     (set to HIGH for forward, LOW for backwards)
Channel B Brake--------------8      (set to HIGH to engage, LOW to release)
Channel B Speed--------------11     (Set to 1-160)
Channel B Current Sensing----A1

ping sensor------------------Arduino
------------------------------------
Control pin------------------A0
+5v--------------------------A1
grnd-------------------------A2

qti sensor-------------------Arduino
------------------------------------
grnd-------------------------5
control pin------------------6
+5v--------------------------7
NOTE: Red wire on QTI sensor is data, NOT +5v!!!
*/
// Include libraries for xbee shield
#include <SoftwareSerial.h>

//Define the range that the target should be away from your bot.
//Define the minimum distance the target can be
#define minDistance 1
//Define the maximum distance the target can be
#define maxDistance 24

//Define the range that the color for the arena will be
//Define the minimum color for the playing field
#define minColor 100
//Define the maximum color for the playing field
#define maxColor 1800

// Begin configuration for QTI sensor
#define qtiGndPin A5
#define qti5vPin A3
#define qti_pin A4

// Configuration for the ping sensor
#define pingGndPin 5
#define ping5vPin 6
#define pingPin 7
unsigned int duration, inches;

// XBee's DOUT (TX) is connected to pin 2 (Arduino's Software RX)
// XBee's DIN (RX) is connected to pin 3 (Arduino's Software TX)
SoftwareSerial XBee(2, 3); // RX, TX
boolean fightStatus = false;

void setup()
{ 
  // For debug
  XBee.begin(9600);
  Serial.begin(9600);

  //Setup Channel A
  pinMode(12, OUTPUT); //Initiates Motor Channel A pin
  pinMode(9, OUTPUT); //Initiates Brake Channel A pin
  digitalWrite(9, HIGH);   //Engage motorRight brake

  //Setup Channel B
  pinMode(13, OUTPUT); //Initiates Motor Channel A pin
  pinMode(8, OUTPUT);  //Initiates Brake Channel A pin
  digitalWrite(8, HIGH);   //Engage motorLeft brake 

  // Set the pins to be used for the qti sensor for power
  pinMode(qti5vPin, OUTPUT);
  pinMode(qtiGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(qti5vPin, HIGH);
  digitalWrite(qtiGndPin, LOW);
  
  // Set the pins to be used for the ping sensor for power
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);
  
  Serial.println("Done with setup, waiting for command to begin.");
  XBee.println("Motors: Done with setup, waiting for command to begin.");
}
void loop()
{
  if (XBee.find("fight")) {
    fightStatus = true;
    XBee.println("Motors: Time to Fight!");
    Serial.println("Motors: Time to Fight!");
  }
  if (fightStatus == true) {
    int cur_distance = ping_distance();
    Serial.print("Current distance: ");
    Serial.println(cur_distance);
    XBee.print("Motors: Current distance: ");
    XBee.println(cur_distance);
    if ((cur_distance > minDistance) && (cur_distance < maxDistance)){
      // Delay half a second to try to get the target more directly in front of the plow
      Serial.println("Pushing target");
      XBee.println("Motors: Pushing target");
      pushTarget();
      Serial.println("Backing up");
      XBee.println("Motors: Backing up");
      backup();
      // Turn a little just in case the target didn't fall
      motorRight(123,0);
      motorLeft(123,1);
      delay(500);
    }
    else{
      motorRight(123,0);
      motorLeft(123,1);
    }
    delay(50);
    if (XBee.find("stop")) {
      fightStatus = false;
      XBee.println("Motors: Stand down.");
      Serial.println("Motors: Stand down.");
    }
  }
}

// Find the distance using the ping sensor.
int ping_distance(){
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              // with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  return inches;                     // Return the results
  //If you want cm, comment out above line and uncomment this one
  //return (inches*2.54);
}

// Backup the robot so we can go again.
void backup() {
  // Stop all motors
  digitalWrite(9, HIGH);   //Engage motorRight brake
  digitalWrite(8, HIGH);   //Engage motorLeft brake 
  // Backup
  motorRight(255,0);
  motorLeft(255,0);
  delay(500);
  // Stop all motors
  digitalWrite(9, HIGH);   //Engage motorRight brake
  digitalWrite(8, HIGH);   //Engage motorLeft brake 
}

// Push the target until the qti sensor finds the edge of the arena
void pushTarget(){
  // Stop all motors
  motorRight(0,0);
  motorLeft(0,0);
  Serial.print("QTI Reading: ");
  Serial.println(RCTime(qti_pin));
  XBee.print("Motors: QTI Reading: ");
  XBee.println(RCTime(qti_pin));
  // Go forward so long as the qti sensor doesn't read the edge
  while ((RCTime(qti_pin) > minColor) && (RCTime(qti_pin) < maxColor)) {
    Serial.print("QTI Reading: ");
    Serial.println(RCTime(qti_pin));
    XBee.print("Motors: QTI Reading: ");
    XBee.println(RCTime(qti_pin));
    // Move forward
    motorRight(255,1);
    motorLeft(255,1);
  }
  // Stop all motors
  digitalWrite(9, HIGH);   //Engage motorRight brake
  digitalWrite(8, HIGH);   //Engage motorLeft brake 
}

void motorRight(int power, int way){
  if (way == 0){
    digitalWrite(12, LOW);    //Backwards
  } else {
    digitalWrite(12, HIGH);    //Forward
  }
  digitalWrite(9, LOW);   //Disengage the Brake for Channel A
  analogWrite(3, power);   //Spins the motor on Channel A at full speed
}

void motorLeft(int power, int way){
  if (way == 0){
    digitalWrite(13, LOW);    //Backwards
  } else {
    digitalWrite(13, HIGH);    //Forward
  }
  digitalWrite(8, LOW);   //Disengage the Brake for Channel A
  analogWrite(11, power);   //Spins the motor on Channel A at full speed
}

// Find time for light to travel back to qti sensor
long RCTime(int sensorIn){
   long duration = 0;
   pinMode(sensorIn, OUTPUT);     // Make pin OUTPUT
   digitalWrite(sensorIn, HIGH);  // Pin HIGH (discharge capacitor)
   delay(1);                      // Wait 1ms
   pinMode(sensorIn, INPUT);      // Make pin INPUT
   digitalWrite(sensorIn, LOW);   // Turn off internal pullups
   while(digitalRead(sensorIn)){  // Wait for pin to go LOW
      duration++;
   }
   return duration;
}
