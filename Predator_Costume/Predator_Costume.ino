#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_Simple_AHRS.h>
#include <PWMServo.h>
/*
Notes:
Below are the pinouts for the i2c pins on each arduino board needed to connect to multiplexer
Board             I2C / TWI pins
Uno, Ethernet     A4 (SDA), A5 (SCL)
Mega2560          20 (SDA), 21 (SCL)
Leonardo          2 (SDA), 3 (SCL)
Due               20 (SDA), 21 (SCL)

Sensor i2c addresses
Adafruit 9DoF:
L3GD20H (0x6A or 0x6B)
LSM303 (0x19 for accelerometer and 0x1E for magnetometer

Parallax Compass
MMA7455  (0x1C or 0x1D)
Memsic 2125 

Because 1E is shared, we need to use the multiplexer for the i2c bus
*/
// This function is to be able to switch between i2c devices
#define TCAADDR 0x70            // 7-bit address of the TCA9548A multiplexer
#define Addr 0x1E               // 7-bit address of the HMC5883 compass 
void tcaselect(uint8_t i) {
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;            // the number of the pushbutton pin
const int ledPin =  13;             // the number of the LED pin
const int speakerPin = 8;           // positive lead pin for the speaker
const int Z_SERVO_CONTROL_PIN = 9;  // Handle rotation servo control
const int Y_SERVO_CONTROL_PIN = 10; // Handle tilt servo control
const int compassIO = 1;            // Multiplexer io connected to compass module
const int gyroIO = 7;               // multiplexer io connected to 9dof module
const int zWiggle = 5;
const int yWiggle = 3;
// variables will change:
int buttonState = 0;          // variable for reading the pushbutton status
int oldZDifference = 0;        // variable used to help reduce stuttering of the servos
int oldYDifference = 0;        // variable used to help reduce stuttering of the servos

// Create sensor instances for 9 DoF.
Adafruit_LSM303_Accel_Unified accel(30301);
Adafruit_LSM303_Mag_Unified   mag(30302);

// Create simple AHRS algorithm using the above sensors.
Adafruit_Simple_AHRS          ahrs(&accel, &mag);

PWMServo z_servo;
PWMServo y_servo;

void setup() 
{
  Serial.begin(115200);

  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  // Initialize the parallax compass module.
  tcaselect(compassIO);
  Wire.begin();      
  // Set operating mode to continuous   
  Wire.beginTransmission(Addr);    
  Wire.write(byte(0x02));   
  Wire.write(byte(0x00));   
  Wire.endTransmission(); 

  // Initialize the 9 dof sensor.
  tcaselect(gyroIO);
  accel.begin();
  mag.begin();
  
  // Initialize the servos.
  z_servo.attach(Z_SERVO_CONTROL_PIN);
  y_servo.attach(Y_SERVO_CONTROL_PIN);

  Serial.println("Done with setup, moving on.");
}

void loop(){
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
    // Make our sound effect
    for (int thisNote = 1500; thisNote > 0; thisNote--) {
      tone(speakerPin, thisNote, 1);
    }
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
  
  // Compass module
  tcaselect(compassIO);
  int x, y, z; 
  // Initiate communications with compass   
  Wire.beginTransmission(Addr);   
  Wire.write(byte(0x03));       
  // Send request to X MSB register   
  Wire.endTransmission(); 
  Wire.requestFrom(Addr, 6);    
  // Request 6 bytes; 2 bytes per axis   
  if(Wire.available() <=6) {    
    // If 6 bytes available     
    x = Wire.read() << 8 | Wire.read();
    z = Wire.read() << 8 | Wire.read();
    y = Wire.read() << 8 | Wire.read();  
  }      
  // If compass module lies flat on the ground with no tilt,
  // just x and y are needed for calculation
  float heading=atan2(x, y)/0.017453292;//5;
  if(heading < 0) heading+=360;
  heading=360-heading; // N=0/360, E=90, S=180, W=270
  Serial.print("Shoulder heading: ");
  Serial.println(heading);  

  // 9dof module
  tcaselect(gyroIO);
  sensors_vec_t   orientation;

  // Use the simple AHRS function to get the current orientation.
  ahrs.getOrientation(&orientation);
  Serial.print("Head heading: ");
  if(orientation.heading > 0) orientation.heading-=360;   // Need this line so we can an accurate compass heading instead of +/- 0-180
  Serial.println(abs(orientation.heading));
  //Serial.println(orientation.heading);
  
  // Servo control
  Serial.println("#########################");
  
  Serial.print("Heading difference: ");
  Serial.print(int(heading-abs(orientation.heading)));
  int zDifference = int((heading-abs(orientation.heading))-oldZDifference);
  if (abs(zDifference) <= zWiggle){
    Serial.println("");
  }else{
    Serial.println(" Moving!");
    z_servo.write(90+(heading-abs(orientation.heading)));
    oldZDifference = heading-abs(orientation.heading);
  };
  
  Serial.print("Head tilt: ");
  Serial.print(int(orientation.pitch));
  int yDifference = int(orientation.pitch-oldYDifference);
  if (abs(yDifference) <= yWiggle){
    Serial.println("");
  }else{
    Serial.println(" Moving!");
    y_servo.write(90+orientation.pitch);
    oldYDifference = int(orientation.pitch);
  };
  Serial.println("#########################");
  delay(100); // If we go too fast, the compass module can't keep up
}
