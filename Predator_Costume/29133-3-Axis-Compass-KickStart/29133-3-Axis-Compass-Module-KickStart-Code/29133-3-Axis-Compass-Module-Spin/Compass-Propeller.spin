OBJ

    pst       :   "FullDuplexSerial"

CON

  _clkmode      = xtal1 + pll16x
  _clkfreq      = 80_000_000

  datapin  = 1         ' SDA of gyro to pin P1 
  clockPin = 0         ' SCL of gyro to pin P0
 
  WRITE_DATA     = $3C ' Requests Write operation 
  READ_DATA      = $3D ' Requests Read operation
  MODE           = $02 ' Mode setting register
  OUTPUT_X_MSB   = $03 ' X MSB data output register  

VAR

  long x
  long y
  long z
         
PUB Main

  waitcnt(clkfreq/100_000 + cnt)     ' Power up delay
  pst.start(31, 30, 0, 115200)
  SetCont
  repeat
    SetPointer(OUTPUT_X_MSB)
    getRaw                           ' Gather raw data from compass       
    pst.tx(1)
    ShowVals

PUB SetCont
  ' Sets compass to continuous output mode
 
  start
  send(WRITE_DATA)
  send(MODE)
  send($00)
  stop

PUB SetPointer(Register)
  ' Start pointer at user specified register (OUT_X_MSB)

  start
  send(WRITE_DATA)
  send(Register)
  stop

PUB GetRaw 
  ' Get raw data from continous output

  start
  send(READ_DATA)
  x := ((receive(true) << 8) | receive(true))
  z := ((receive(true) << 8) | receive(true))
  y := ((receive(true) << 8) | receive(false))
  stop
  ~~x
  ~~z
  ~~y
  x := x       
  z := z 
  y := y 

PUB ShowVals
  ' Display XYZ compass values
        
  pst.str(string("X="))        
  pst.dec(x)
  pst.str(string(", Y="))
  pst.dec(y)
  pst.str(string(", Z="))
  pst.dec(z)
  pst.str(string("    ")) 

PRI send(value)

  value := ((!value) >< 8)

  repeat 8
    dira[dataPin]  := value
    dira[clockPin] := false
    dira[clockPin] := true
    value >>= 1

  dira[dataPin]  := false
  dira[clockPin] := false
  result         := !(ina[dataPin])
  dira[clockPin] := true
  dira[dataPin]  := true

PRI receive(aknowledge)

  dira[dataPin] := false

  repeat 8
    result <<= 1
    dira[clockPin] := false
    result         |= ina[dataPin]
    dira[clockPin] := true

  dira[dataPin]  := aknowledge
  dira[clockPin] := false
  dira[clockPin] := true
  dira[dataPin]  := true

PRI start

  outa[dataPin]  := false
  outa[clockPin] := false
  dira[dataPin]  := true
  dira[clockPin] := true

PRI stop

  dira[clockPin] := false
  dira[dataPin]  := false