#include <LiquidCrystal.h>
// Tell the Arduino you want to use the LCD library

LiquidCrystal lcd(12, 11, 10, 9, 8, 7);
int photocellPin = 0;     // the cell and 10K pulldown are connected to a0
int photocellReading;     // the analog reading from the sensor divider

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16,2);
  Serial.begin(9600);   
}

void loop() {
  photocellReading = analogRead(photocellPin);  
  lcd.setCursor(0, 0);
  lcd.print("LUX = ");
  lcd.setCursor(6, 0);
  lcd.print(photocellReading);

  delay(400);
 
  // clear screen for the next loop:
  //lcd.clear();
}
