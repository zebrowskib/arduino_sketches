/*
Reference for continues rotation servos
1700  =  Counter-Clockwise
1300  =  Clockwise
1500  =  Stationary
Anything in between = speed

Turning degrees seems to be close to 600 delay = 90 degree turn.

Here are the connections necessary:
servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10
golf arm---------------------D12

ping sensor------------------Arduino
------------------------------------
gnd pin----------------------A1
power pin--------------------A2
control pin------------------A3

*/
// Include libraries for Servo
#include <Servo.h>

//Define how long before the arm swings when it finds its target
#define countdown_time 10
//Define the range that the target should be away from your bot.
//Define the minimum distance the target can be
#define minDistance 24
//Define the maximum distance the target can be
#define maxDistance 110

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
#define arm_servo_pin 12
Servo servoRight;
Servo servoLeft;
Servo servoArm;

// Configuration for the ping sensor
#define pingGndPin A1
#define ping5vPin A2
#define pingPin A3
unsigned int duration, inches;

// Finially configure the lcd push button shield
#include <LiquidCrystal.h>
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);
 
void setup()
{
  // For debug, but you can use the lcd instead.
  Serial.begin(9600);
  
  // Setup the servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
  servoArm.attach(arm_servo_pin); 
  
  // Set the pins to be used for the ping sensor to output for digitalwrite
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);
  // Set the arm to the start position to begin
  servoArm.write(170);
  
  lcd.begin(16, 2);
  lcd.clear();
}
void loop()
{
  lcd.setCursor(0,0);
  lcd.print("Current Dist:");
  lcd.setCursor(13,0);
  int cur_distance = ping_distance();
  lcd.print(cur_distance);

  if ((cur_distance > minDistance) && (cur_distance < maxDistance)){
    lcd.setCursor(0,1);
    lcd.print("Ready to fire!");
    delay(2000);
    // Stop servos
    servoRight.writeMicroseconds(1500);
    servoLeft.writeMicroseconds(1500);
    
    count_down();
    swing_arm();
  }
  else{
    // Rotate Right
    servoRight.writeMicroseconds(1450);
    servoLeft.writeMicroseconds(1450);
    delay(20);
    servoRight.writeMicroseconds(1500);
    servoLeft.writeMicroseconds(1500);
    // Stop
  }
   
  delay(200);
  lcd.clear();
    
}

// Find the distance using the ping sensor.
int ping_distance(){
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              //   with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  return inches;                     // Return the results
}

// As the name implies, swing the servo arm.
void swing_arm(){
  servoArm.write(170);
  delay(1000);
  servoArm.write(10);
  delay(1000);
  servoArm.write(170);
}

// Display on the lcd a countdown before we swing the arm so we can put the ball in place
void count_down(){
  for (int i = countdown_time; i > 0; i--){
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Ready to fire in");
    lcd.setCursor(0,1);
    lcd.print(i);
    lcd.setCursor(3,1);
    lcd.print("seconds");
    delay(1000);
  }
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("!FIRE!FIRE!FIRE!");
}
