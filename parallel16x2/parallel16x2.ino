#include <LiquidCrystal.h>
// Tell the Arduino you want to use the LCD library

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16,2);

}

void loop() {
  lcd.setCursor(17, 0);
  
  char char_txt[52]="This is some text that we are trying to display.";
  lcd.print(char_txt);
  for (int char_txt = 0; char_txt < 32; char_txt++) {
   lcd.scrollDisplayLeft(); 
   lcd.print(char_txt);
   delay(400);
  }
 
  // clear screen for the next loop:
  lcd.clear();
}
