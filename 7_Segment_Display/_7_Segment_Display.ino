
void setup() {              
  pinMode(2, OUTPUT); 
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(9, 1);  // start with the "dot" off
}

//Define all characters you would want to print
void char_A()
{
  digitalWrite(2, 0);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 1);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_B()
{
  digitalWrite(2, 0);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 0);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_C()
{
  digitalWrite(2, 0);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 0);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 1);
}
void char_D()
{
  digitalWrite(2, 0);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 0);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 1);
}
void char_E()
{
  digitalWrite(2, 0);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 0);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_F()
{
  digitalWrite(2, 0);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 1);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_G()
{
  digitalWrite(2, 0);
  digitalWrite(3, 1);
  digitalWrite(4, 0);
  digitalWrite(5, 0);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_H()
{
  digitalWrite(2, 1);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 1);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 0);
}
void char_J()
{
  digitalWrite(2, 1);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 0);
  digitalWrite(6, 1);
  digitalWrite(7, 1);
  digitalWrite(8, 1);
}
void char_TOP()
{
  digitalWrite(2, 0);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 1);
  digitalWrite(6, 1);
  digitalWrite(7, 1);
  digitalWrite(8, 1);
}
void char_RIGHT()
{
  digitalWrite(2, 1);
  digitalWrite(3, 0);
  digitalWrite(4, 0);
  digitalWrite(5, 1);
  digitalWrite(6, 1);
  digitalWrite(7, 1);
  digitalWrite(8, 1);
}
void char_BOTTOM()
{
  digitalWrite(2, 1);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 0);
  digitalWrite(6, 1);
  digitalWrite(7, 1);
  digitalWrite(8, 1);
}
void char_LEFT()
{
  digitalWrite(2, 1);
  digitalWrite(3, 1);
  digitalWrite(4, 1);
  digitalWrite(5, 1);
  digitalWrite(6, 0);
  digitalWrite(7, 0);
  digitalWrite(8, 1);
}
void char_9()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 1);
 digitalWrite(6, 1);
 digitalWrite(7, 0);
 digitalWrite(8, 0);
}
void char_8()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 0);
 digitalWrite(6, 0);
 digitalWrite(7, 0);
 digitalWrite(8, 0);
}
void char_7()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 1);
 digitalWrite(6, 1);
 digitalWrite(7, 1);
 digitalWrite(8, 1);
}
void char_6()
{
 digitalWrite(2, 0);
 digitalWrite(3, 1);
 digitalWrite(4, 0);
 digitalWrite(5, 0);
 digitalWrite(6, 0);
 digitalWrite(7, 0);
 digitalWrite(8, 0);
}
void char_5()
{
 digitalWrite(2, 0);
 digitalWrite(3, 1);
 digitalWrite(4, 0);
 digitalWrite(5, 0);
 digitalWrite(6, 1);
 digitalWrite(7, 0);
 digitalWrite(8, 0);
}
void char_4()
{
 digitalWrite(2, 1);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 1);
 digitalWrite(6, 1);
 digitalWrite(7, 0);
 digitalWrite(8, 0);
}
void char_3()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 0);
 digitalWrite(6, 1);
 digitalWrite(7, 1);
 digitalWrite(8, 0);
}
void char_2()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 1);
 digitalWrite(5, 0);
 digitalWrite(6, 0);
 digitalWrite(7, 1);
 digitalWrite(8, 0);
}
void char_1()
{
 digitalWrite(2, 1);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 1);
 digitalWrite(6, 1);
 digitalWrite(7, 1);
 digitalWrite(8, 1);
}
void char_0()
{
 digitalWrite(2, 0);
 digitalWrite(3, 0);
 digitalWrite(4, 0);
 digitalWrite(5, 0);
 digitalWrite(6, 0);
 digitalWrite(7, 0);
 digitalWrite(8, 1);
}
void char_null()
{
 digitalWrite(2, 1);
 digitalWrite(3, 1);
 digitalWrite(4, 1);
 digitalWrite(5, 1);
 digitalWrite(6, 1);
 digitalWrite(7, 1);
 digitalWrite(8, 1);
}
 
 
 
 
void loop() {
 //Test countdown
 char_9();
 delay(1000);
 char_8();
 delay(1000);
 char_7();
 delay(1000);
 char_6();
 delay(1000);
 char_5();
 delay(1000);
 char_4();
 delay(1000);
 char_3();
 delay(1000);
 char_2();
 delay(1000);
 char_1();
 delay(1000);
 char_0();
 delay(1000);
 
 //Test decimal point
 digitalWrite(9, 1);
 delay(100);
 digitalWrite(9, 0); 
 delay(100);
 digitalWrite(9, 1);
 delay(100);
 digitalWrite(9, 0);
 delay(100);
 digitalWrite(9, 1);
 delay(100);
 digitalWrite(9, 0);
 delay(100);
 digitalWrite(9, 1);
 delay(100);
 
 //Test racing around
 char_TOP();
 delay(100);
 char_RIGHT();
 delay(100);
 char_BOTTOM();
 delay(100);
 char_LEFT();
 delay(100);
 char_TOP();
 delay(100);
 char_RIGHT();
 delay(100);
 char_BOTTOM();
 delay(100);
 char_LEFT();
 delay(100);
 char_TOP();
 delay(100);
 char_RIGHT();
 delay(100);
 char_BOTTOM();
 delay(100);
 char_LEFT();
 delay(100);
 char_null();
 delay(4000);

 //Test print characters
 char_H();
 delay(1000);
 char_E();
 delay(1000);
 char_1();
 delay(1000);
 char_null();
 delay(100);
 char_1();
 delay(1000);
 char_0();
 delay(1000);
 char_null();
 delay(4000);
 

}
