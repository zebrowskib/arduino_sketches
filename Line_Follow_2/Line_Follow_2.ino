/*
Reference for continues rotation servos
1700  =  Counter-Clockwise
1300  =  Clockwise
1500  =  Stationary
Anything in between = speed

servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10

QTI Sensor-------------------D12
*/

// Include libraries for Servo
#include <Servo.h>

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
Servo servoRight;
Servo servoLeft;

// Begin configuration for QTI sensor
#define qti_left_pin 8
#define qti_right_pin 9

void setup()
{
  // Setup serial output for debugging
  Serial.begin(9600);                   // Begin the serial monitor at 9600bps
  
  // Setup two servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
}  
 
void loop()
{
  if ((RCTime(qti_left_pin)) < 150)
    // Move forward
    servoRight.writeMicroseconds(1300);
  else
    // Turn off the left servo
    servoRight.writeMicroseconds(1700);
  
  if ((RCTime(qti_right_pin)) < 150)
    // Move forward
    servoLeft.writeMicroseconds(1700);
  else
    // Turn off the left servo
    servoLeft.writeMicroseconds(1300);
}

long RCTime(int sensorIn){
   long duration = 0;
   pinMode(sensorIn, OUTPUT);     // Make pin OUTPUT
   digitalWrite(sensorIn, HIGH);  // Pin HIGH (discharge capacitor)
   delay(1);                      // Wait 1ms
   pinMode(sensorIn, INPUT);      // Make pin INPUT
   digitalWrite(sensorIn, LOW);   // Turn off internal pullups
   while(digitalRead(sensorIn)){  // Wait for pin to go LOW
      duration++;
   }
   return duration;
}
