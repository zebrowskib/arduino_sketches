// Begin configuration for QTI sensor
#define pirGndPin 5
#define pir5vPin 12
#define pir_pin 13

#define xbee5vPin 2
#define xbeeGndPin 3

int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status
int sense_count = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  // Set the pins to be used for the qti sensor to output for digitalwrite
  pinMode(pir5vPin, OUTPUT);
  pinMode(pirGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(pir5vPin, HIGH);
  digitalWrite(pirGndPin, LOW);

  pinMode(xbee5vPin, OUTPUT);
  pinMode(xbeeGndPin, OUTPUT);
  digitalWrite(xbee5vPin, HIGH);
  digitalWrite(xbeeGndPin, LOW);

  delay(3000); // wait a few seconds so that the xbee can boot
  Serial.println("Done with setup, searching for targets");
}
void loop(){
  val = digitalRead(pir_pin);  // read input value
  if (val == HIGH) {            // check if the input is HIGH
    if (pirState == LOW) {
      // we have just turned on
      Serial.print("Motion detected! ");
      Serial.println(sense_count);
      sense_count++;
      // We only want to print on the output change, not state
      pirState = HIGH;
    }
  } else {
    if (pirState == HIGH){
      // we have just turned of
      Serial.println("Motion ended!");
      // We only want to print on the output change, not state
      pirState = LOW;
    }
  }
}
