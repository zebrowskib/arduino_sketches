//Add the SdFat Libraries
#include <SdFat.h>

//Create the variables to be used by SdFat Library
Sd2Card card;
SdVolume volume;
SdFile root;
SdFile file;

char name[] = "Test.txt";     //Create an array that contains the name of our file.
char contents[256];           //This will be a data buffer for writing contents to the file.
char in_char=0;
int index=0;                  //Index will keep track of our position within the contents buffer.

int photocellPin = 0;     // the cell and 10K pulldown are connected to a0
int photocellReading;     // the analog reading from the sensor divider

void setup() {
  Serial.begin(9600);   
  pinMode(10, OUTPUT);       //Pin 10 must be set as an output for the SD communication to work.
    card.init();               //Initialize the SD card and configure the I/O pins.
    volume.init(card);         //Initialize a volume on the SD card.
    root.openRoot(volume);     //Open the root directory in the volume. 
}

void loop() {
  photocellReading = analogRead(photocellPin);  
  file.open(root, name, O_CREAT | O_APPEND | O_WRITE);    //Open or create the file 'name' in 'root' for writing to the end of the file.
  sprintf(contents, "LUX: %d    ", photocellReading);    //Copy the letters 'Millis: ' followed by the integer value of the millis() function into the 'contents' array.
  file.print(contents);    //Write the 'contents' array to the end of the file.
  file.close();            //Close the file.

  delay(400);

}
