/*
Origional Idea: 
http://learn.parallax.com/contest/clear-table

Reference for continues rotation servos
180  =  Counter-Clockwise
0  =  Clockwise
90  =  Stationary
Anything in between = speed

Here are the connections necessary:
servo------------------------Arduino
------------------------------------
left servo-------------------D11
right servo------------------D10

ping sensor------------------Arduino
------------------------------------
grnd-------------------------2
+5v--------------------------3
control pin------------------4

*/
// Include libraries for Servo
#include <Servo.h>

//Define the range that the target should be away from your bot.
//Define the minimum distance the target can be
#define minDistance 1
//Define the maximum distance the target can be
#define maxDistance 25

// Begin configuration for servos
#define left_servo_pin 11
#define right_servo_pin 10
Servo servoRight;
Servo servoLeft;

// Configuration for the ping sensor
#define pingGndPin A2
#define ping5vPin A1
#define pingPin A0
unsigned int duration, inches;

void setup()
{
  // For debug
  Serial.begin(9600);
  
  // Setup the servos
  servoRight.attach(right_servo_pin); 
  servoLeft.attach(left_servo_pin); 
  
  // Set the pins to be used for the ping sensor to output for digitalwrite
  pinMode(ping5vPin, OUTPUT);
  pinMode(pingGndPin, OUTPUT);
  // Now set them to 5v and ground as applicable
  digitalWrite(ping5vPin, HIGH);
  digitalWrite(pingGndPin, LOW);
  
  Serial.println("Done with setup, searching for targets");
}
void loop()
{
  int cur_distance = ping_distance();
  Serial.print("Current distance: ");
  Serial.println(cur_distance);
  if ((cur_distance > minDistance) && (cur_distance < maxDistance)){
    Serial.println("Pushing target");
    // Move forward
    servoRight.write(0);
    servoLeft.write(180);
  }
  else{
    Serial.println("Searching for target");
    // Turn
    servoRight.write(180);
    servoLeft.write(180);
  }
  delay(50);
}

// Find the distance using the ping sensor.
int ping_distance(){
  pinMode(pingPin, OUTPUT);          // Set pin to OUTPUT
  digitalWrite(pingPin, LOW);        // Ensure pin is low
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);       // Start ranging
  delayMicroseconds(5);              // with 5 microsecond burst
  digitalWrite(pingPin, LOW);        // End ranging
  pinMode(pingPin, INPUT);           // Set pin to INPUT
  duration = pulseIn(pingPin, HIGH); // Read echo pulse
  inches = duration / 74 / 2;        // Convert to inches
  return inches;                     // Return the results
  //If you want cm, comment out above line and uncomment this one
  //return (inches*2.54);
}
