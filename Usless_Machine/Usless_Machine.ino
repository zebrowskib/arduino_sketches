#include <Servo.h>
#include <avr/sleep.h>
Servo myservo2;  // create servo object to control a servo
Servo myservo1; 
                
int pos = 0;    // variable to store the servo position
int boxon =1;
// Predefined positions for a spesific box/servo
int S1fra = 1400;    // from   s1=Servo1
int S1mid = 1800;    // Mid
int S1mid2 = 1580;
int S1mid3 = 1640;
int S1til = 2050;    // To
int S2fra = 1784;    //From    s2=Servo2
int S2mid = 1200;
int S2mid2 = 1000;   // Almost on the switch
int S2til = 770;
　
int seq = 0;
 
void setup()
{
  myservo2.attach(13);  // attaches the servo on pin 13 to the servo object (PB5)
  myservo1.attach(12);  // attaches the servo on pin 12 to the servo object (PB4)
  pinMode(8, OUTPUT);   // For the MOSFET
  pinMode(2, INPUT);    // For the switch interrupt
  digitalWrite(2, HIGH);
  myservo2.write(S2fra);
  myservo1.write(S1fra);
  delay(600);
  digitalWrite(8, HIGH);
  delay(300);
  digitalWrite(8, LOW);
// Put all unused pins to input high to save power.
  pinMode(3, INPUT);
  digitalWrite(3, HIGH);
  pinMode(4, INPUT);
  digitalWrite(4, HIGH);
  pinMode(5, INPUT);
  digitalWrite(5, HIGH);
  pinMode(6, INPUT);
  digitalWrite(6, HIGH);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);
  pinMode(9, INPUT);
  digitalWrite(9, HIGH);
  pinMode(10, INPUT);
  digitalWrite(10, HIGH);
  pinMode(11, INPUT);
  digitalWrite(11, HIGH);
  DDRC = 0;               //Analog input 1/6 (PortC) set to input high also
  PORTC = 63;
.   
     //External interrupt INT0
  EICRA=0;   //The low level of INT0 generates an interrupt request
  EIMSK=1;   //External Interrupt Request 0 Enable
 
}
 
 
void loop()
{
  if(!boxon) //boxon should actually be called boxoff. Did a mistake here.
  {
    delay(500);
    digitalWrite(8, HIGH);
    //seq = int(random(0,10)); //If you want random…
   
    if(seq == 0)Sequense3();
    if(seq == 1)Sequense1();
    if(seq == 2)Sequense3();
    if(seq == 3)Sequense9();
    if(seq == 4)Sequense3();
    if(seq == 5)Sequense5();
    if(seq == 6)Sequense3();
    if(seq == 7)Sequense7();
    if(seq == 8)Sequense3();
    if(seq == 9)Sequense2();
    if(seq == 10)Sequense3();
    if(seq == 11)Sequense10();
    if(seq == 12)Sequense3();
    if(seq == 13)Sequense8();
    if(seq == 14)Sequense3();
    if(seq == 15)Sequense6();
    if(seq == 16)Sequense3();
    if(seq == 17)Sequense4();
    seq++;
    if(seq>17) seq=0;
    delay(100);
    digitalWrite(8, LOW);
    boxon=digitalRead(2);
  }
  else
  {
    //Set sleep mode, turn off MOSFET and servos
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    pinMode(12, INPUT);
    digitalWrite(12, HIGH);
    pinMode(13, INPUT);
    digitalWrite(13, HIGH);
    PRR = 255;
    MCUCR |= (1<<BODS) | (1<<BODSE);
    MCUCR &= ~(1<<BODSE);   
    EIMSK=1;
    sleep_mode();
    // ZZZzzz…
    sleep_disable();     //Awake again…
    PRR = 0;
    pinMode(12, OUTPUT);
    pinMode(13, OUTPUT);
  }
  boxon=digitalRead(2); //If pin 2 is low, box is on and bonxon=flase
}
　
//Fra=From, Til=To (Sorry for some Norwegian variables)
void Sweep(int srv, int fra, int til, int usec) 
{
  if(srv == 1)
  {
    if(fra <= til)
      for(pos = fra; pos < til; pos += 1)
      {
        myservo1.writeMicroseconds(pos);
        delayMicroseconds(usec);
      }
    else
    {
      for(pos = fra; pos>=til; pos-=1)
      {
         myservo1.writeMicroseconds(pos);
        delayMicroseconds(usec);
      }
    }
  }
  if(srv == 2)
  {
    if(fra <= til)
      for(pos = fra; pos < til; pos += 1)
      {
        myservo2.writeMicroseconds(pos);
        delayMicroseconds(usec);
      }
    else
    {
      for(pos = fra; pos>=til; pos-=1)
      {
         myservo2.writeMicroseconds(pos);
        delayMicroseconds(usec);
      }
    }
  }
}
　
ISR(INT0_vect)   // Step rising edge interrupt. Switch flipped.
{
  EIMSK=0;   //Turn off interrupt
  boxon=digitalRead(2);  //Read the switch a couple of times to avoid a nasty non working thing some times.
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
  boxon=digitalRead(2);
}
void Sequense1()
{
  delay(700);
  Sweep(1, S1fra, S1mid, 3000);
  delay(1000);
  Sweep(1, S1mid, S1fra, 500);
  delay(1000);
  Sweep(1, S1fra, S1til, 1000);
  Sweep(2, S2fra, S2mid, 1800);
  Sweep(2, S2mid, S2til, 500);
  delay(100);
  Sweep(2, S2til, S2fra, 500);
  Sweep(1, S1til, S1fra, 500);
}
void Sequense2()
{
  delay(800);
  Sweep(1, S1fra, S1mid2, 3000);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(120);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(120);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(120);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(120);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(120);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(120);
  Sweep(1, S1mid2, S1fra, 3000);
  Sweep(1, S1fra, S1mid, 3000);
  delay(1000);
  Sweep(1, S1mid, S1til, 1000);
  Sweep(2, S2fra, S2mid, 1800);
  Sweep(2, S2mid, S2til, 500);
  delay(100);
  Sweep(2, S2til, S2fra, 500);
  Sweep(1, S1til, S1fra, 500);
}
void Sequense3()
{
  delay(50);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense4()
{
  delay(500);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2mid2, 1);
  delay(450);
  Sweep(2, S2mid2, S2til, 30000);
  delay(1);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense5()
{
  delay(1000);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(2, S2til, S2mid2, 1);
  delay(110);
  Sweep(2, S2mid2, S2til, 1);
  delay(110);
  Sweep(2, S2til, S2mid2, 1);
  delay(110);
  Sweep(2, S2mid2, S2til, 1);
  delay(110);
  Sweep(2, S2til, S2mid2, 1);
  delay(110);
  Sweep(2, S2mid2, S2til, 1);
  delay(110);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense6()
{
  delay(1500);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(1, S1til, S1fra, 1000);
  delay(2000);
  Sweep(1, S1fra, S1til, 1000);
  delay(2000);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense7()
{
  delay(500);
  Sweep(1, S1fra, S1mid, 1);
  delay(200);
  Sweep(1, S1mid, S1mid2, 1);
  delay(100);
  Sweep(1, S1mid2, S1mid, 1);
  delay(100);
  Sweep(1, S1mid, S1mid2, 1);
  delay(100);
  Sweep(1, S1mid2, S1mid, 1);
  delay(100);
  Sweep(1, S1mid, S1fra, 1);
  delay(200);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense8()
{
  delay(200);
  Sweep(1, S1fra, S1mid, 1);
  delay(200);
  Sweep(1, S1mid, S1mid2, 1);
  delay(100);
  Sweep(1, S1mid2, S1mid, 1);
  delay(100);
  Sweep(1, S1mid, S1mid2, 1);
  delay(100);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1fra, 1);
  delay(200);
  Sweep(1, S1fra, S1til, 1);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense9()
{
  delay(1000);
  Sweep(1, S1fra, S1mid, 2000);
  delay(500);
  Sweep(1, S1mid, S1mid2, 1000);
  delay(1);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(50);
  Sweep(1, S1mid2, S1mid3, 1);
  delay(50);
  Sweep(1, S1mid3, S1mid2, 1);
  delay(500);
  Sweep(1, S1mid2, S1mid, 5000);
  delay(1);
  Sweep(1, S1mid, S1til, 1000);
  delay(1);
  Sweep(2, S2fra, S2til, 1);
  delay(450);
  Sweep(2, S2til, S2fra, 1);
  delay(200);
  Sweep(1, S1til, S1fra, 1);
  delay(400);
}
void Sequense10()
{
  delay(800);
  Sweep(1, S1fra, S1til, 30000);
  delay(1);
  Sweep(2, S2fra, S2til, 3000);
  delay(1);
  Sweep(2, S2til, S2fra, 3000);
  delay(1);
  Sweep(1, S1til, S1mid, 30000);
  delay(1);
  Sweep(1, S1mid, S1fra, 1);
  delay(300);

}
